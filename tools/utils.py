import sys

import tensorflow as tf
import os
import json
from baselines.common import mpi_config
useMPI=mpi_config.try_import_mpi()
if useMPI:
    from mpi4py import MPI
import datetime
import numpy as np

import os
result_path = os.path.dirname(os.path.abspath(__file__)) + '/../results/'


def get_session():
    return tf.get_default_session()

def clear_state(identifier):
    if os.path.exists(result_path + 'saved_model/model_data/'):
        for f in os.listdir(result_path + 'saved_model/model_data/'):
            if f.startswith(identifier):
                os.remove(result_path + 'saved_model/model_data/'+f)

def load_state(identifier, iters_so_far=None):
    if iters_so_far is not None:
        fname = result_path + 'saved_model/model_data/' + identifier + '_' + str(iters_so_far) + '.ckpt'
    else:
        fname = result_path + 'saved_model/model_data/' + identifier + '.ckpt'
    if not os.path.exists(fname) and not os.path.exists(fname+'.meta'):
        latest_iter=0
        for f in os.listdir(result_path+'saved_model/model_data/'):
            if f.startswith(identifier + '_'):
                iter=f[len(identifier)+1:].split('.')[0]
                if iter.isdigit():
                    iter=int(iter)
                    if iter > latest_iter:
                        latest_iter=iter
        latest = result_path + 'saved_model/model_data/' + identifier + '_' + str(latest_iter) + '.ckpt'
        print('Cannot find: %s using latest state: %s!'%(fname,latest))
        fname=latest
    saver = tf.train.Saver(var_list=tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope="^((?!next_).)*$"))
    #tf.train.import_meta_graph(fname+'.meta')
    saver.restore(get_session(), fname)


def load_next_state(identifier, iters_so_far):
    fname = result_path + 'saved_model/experts/' + identifier + '_' + str(iters_so_far) + '.ckpt'
    saver = tf.train.Saver(var_list=tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='next_' + identifier))
    saver.restore(get_session(), fname)


def save_state(identifier, iters_so_far):
    fname = result_path + 'saved_model/model_data/' + identifier + '_' + str(iters_so_far) + '.ckpt'
    os.makedirs(os.path.dirname(fname), exist_ok=True)
    saver = tf.train.Saver()
    saver.save(get_session(), fname)


def save_params(params, env_id):
    save_path = result_path + "saved_model/params/"
    os.makedirs(save_path, exist_ok=True)
    with open(save_path + env_id + ".json", "w") as f:
        json.dump(params, f)


def load_params(env_id):
    with open(result_path + "saved_model/params/" + env_id + ".json", "r") as f:
        params = dict(json.load(f))
    return params


def save_rewards(reward_list, identifier, iters_so_far):
    reward_list = np.array(reward_list).tolist()
    save_path = result_path + "saved_model/reward_jsons/"
    os.makedirs(save_path, exist_ok=True)
    with open(save_path + identifier + '_' + str(iters_so_far) + ".json", "w") as f:
        json.dump(reward_list, f)


def load_rewards(identifier, iters_so_far):
    load_path = result_path + "saved_model/reward_jsons/"
    try:
        with open(load_path + identifier + '_' + str(iters_so_far) + '.json', 'r') as f:
            reward_list = json.load(f)
    except FileNotFoundError:
        reward_list = list()
    return reward_list


def tensorboard_summary(name, value, ite):
    pass


def clear_init_state(identifier):
    if os.path.exists(result_path + 'saved_state/'):
        for f in os.listdir(save_path):
            if f.startswith(identifier):
                os.remove(save_path + f)
