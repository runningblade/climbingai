import tensorflow as tf
import multiprocessing
import sys
import argparse

import dagger


def main():
    parser = argparse.ArgumentParser(description='Run dagger')
    parser.add_argument('--id', type=str, default='student')
    parser.add_argument('--p', type=float, default=None)
    parser.add_argument('--T', type=int, default=int(5e3))
    parser.add_argument('--N', type=int, default=int(1e6))
    parser.add_argument('--batch_size', type=int, default=int(1e4))
    parser.add_argument('--dataset_size', type=int, default=int(1e5))
    parser.add_argument('--expert_ids', type=str, nargs='+', required=True)
    parser.add_argument('--expert_iters', type=str, nargs='+', required=True)
    parser.add_argument('--expert_weights', type=float, nargs='+', default=None)
    parser.add_argument('--expert_net_size', type=int, nargs='+', default=(128, 64))
    parser.add_argument('--student_net_size', type=int, nargs='+', default=(256, 128, 64))
    parser.add_argument('--optim_epoch', type=int, default=20)
    parser.add_argument('--optim_stepsize', type=float, default=3e-3)
    parser.add_argument('--adam_epsilon', type=float, default=1e-5)
    parser.add_argument('--render', default=False, action='store_true')
    parser.add_argument('--save_interval', type=int, default=10)
    parser.add_argument('--tb_logdir', type=str, default=None)
    parser.add_argument('--seed', type=int, default=0)
    parser.add_argument('--cont_iter', type=int, default=None)
    parser.add_argument('--init_state_ids', type=str, nargs='+', default=None)
    parser.add_argument('--init_state_iters', type=int, nargs='+', default=None)
    args = parser.parse_args()
    dict_args = vars(args)
    dict_args['identifier'] = dict_args['id']
    del dict_args['id']

    #tf configs
    ncpu = multiprocessing.cpu_count()
    if sys.platform == 'darwin': ncpu //= 2
    config = tf.ConfigProto(allow_soft_placement=True,
                            intra_op_parallelism_threads=ncpu,
                            inter_op_parallelism_threads=ncpu)
    config.gpu_options.allow_growth = True
    tf.Session(config=config).__enter__()

    dagger.learn(**dict_args)


if __name__ == '__main__':
    main()
