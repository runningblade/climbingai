from baselines.common.mpi_running_mean_std import RunningMeanStd
import baselines.common.tf_util as U
import tensorflow as tf
import gym
from baselines.common.distributions import make_pdtype


class MlpPolicy(object):
    recurrent = False
    def __init__(self, name, *args, **kwargs):
        with tf.variable_scope(name):
            self._init(*args, **kwargs)
            self.scope = tf.get_variable_scope().name

    def _init(self, ob_space, ac_space, hid_layer_sizes, gaussian_fixed_var=True, reduce_std=False, ob_ph=None, ob_prefix=''):
        assert isinstance(ob_space, gym.spaces.Box)

        self.pdtype = pdtype = make_pdtype(ac_space)
        sequence_length = None

        if ob_ph is None:
            ob = U.get_placeholder(name=ob_prefix + "ob", dtype=tf.float32, shape=[sequence_length] + list(ob_space.shape))
        else:
            ob = ob_ph

        with tf.variable_scope("obfilter"):
            self.ob_rms = RunningMeanStd(shape=ob_space.shape)

        with tf.variable_scope('vf'):
            obz = tf.clip_by_value((ob - self.ob_rms.mean) / self.ob_rms.std, -5.0, 5.0)
            last_out = obz
            for i, size in enumerate(hid_layer_sizes):
                last_out = tf.nn.relu(tf.layers.dense(last_out, size, name="fc%i" % (i + 1),
                                                      kernel_initializer=U.normc_initializer(1.0)))
            self.vpred = tf.layers.dense(last_out, 1, name='final', kernel_initializer=U.normc_initializer(1.0))[:, 0]

        with tf.variable_scope('pol'):
            '''
            0 abdomen_z
            1 abdomen_y
            2 abdomen_x
            3 right_hip_x
            4 right_hip_z
            5 right_hip_y
            6 right_knee
            7 left_hip_x
            8 left_hip_z
            9 left_hip_y
            10 left_knee
            11 right_shoulder1
            12 right_shoulder2
            13 right_elbow
            14 left_shoulder1
            15 left_shoulder2
            16 left_elbow
            [17:21] (grasp)
            '''
            last_out = obz
            for i, size in enumerate(hid_layer_sizes):
                last_out = tf.nn.relu(tf.layers.dense(last_out, size, name="fc%i" % (i + 1),
                                                      kernel_initializer=U.normc_initializer(1.0)))
            if gaussian_fixed_var and isinstance(ac_space, gym.spaces.Box):
                mean = tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final',
                                       kernel_initializer=U.normc_initializer(0.01))
                if reduce_std:
                    # logstd_ori = tf.get_variable(name="logstd_ori", shape=[1, pdtype.param_shape()[0] // 2 - 7],
                    #                              initializer=tf.zeros_initializer())
                    # logstd = tf.gather(logstd_ori, [0, 1, 2, 3, 4, 5, 6, 3, 4, 5, 6, 7, 8, 9, 7, 8, 9, 10, 11, 12, 13],
                    #                    name='logstd', axis=1)
                    # self.std = logstd_ori
                    logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2],
                                             initializer=tf.zeros_initializer())
                    logstd = logstd * 10
                else:
                    logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2],
                                             initializer=tf.zeros_initializer())
                    self.std = logstd
                pdparam = tf.concat([mean, mean * 0.0 + logstd], axis=1)
            else:
                pdparam = tf.layers.dense(last_out, pdtype.param_shape()[0], name='final',
                                          kernel_initializer=U.normc_initializer(0.01))

        self.pd = pdtype.pdfromflat(pdparam)

        self.state_in = []
        self.state_out = []
        self.mean = mean

        stochastic = tf.placeholder(dtype=tf.bool, shape=())
        ac = U.switch(stochastic, self.pd.sample(), self.pd.mode())
        self._act = U.function([stochastic, ob], [ac, self.vpred])
        self._act_pd = U.function([stochastic, ob], [ac, self.pd.mean, self.pd.std])

    def act(self, stochastic, ob):
        ac1, vpred1 =  self._act(stochastic, ob[None])
        return ac1[0], vpred1[0]

    def act_pd(self, stochastic, ob):
        ac1, mean1, std1 = self._act_pd(stochastic, ob[None])
        return ac1[0], mean1[0], std1[0]

    def get_variables(self):
        return tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.scope)

    def get_trainable_variables(self):
        return tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, self.scope)
        
    def get_initial_state(self):
        return []

    def reset_logstd(self):
        tf.get_default_session().run(tf.variables_initializer([self.std]))

    def augment_logstd(self, coe=2.0):
        import numpy as np
        tf.get_default_session().run(tf.assign_add(self.std, tf.ones_like(self.std) * np.log(coe)))

