import os

def try_import_mpi():
    import configparser
    try:
        config=configparser.ConfigParser()
        config.read(os.path.dirname(os.path.abspath(__file__))+'/MPIConfig.cfg')
        config.sections()
        if not config.has_option('MPI','useMPI'):
            useMPI = True
        else: 
            useMPI = config.getboolean('MPI','useMPI')
        if not useMPI:
            return
        from mpi4py import MPI
        useMPI = True
    except:
        useMPI = False
    return useMPI