import os,sys
from shutil import rmtree
base_dir = os.path.dirname(os.path.abspath(__file__)) + '/../'
sys.path.append(base_dir)
sys.path.append(base_dir + 'simulator/build/')
sys.path.insert(0, os.path.join(base_dir, 'envs'))
sys.path.insert(0, os.path.join(base_dir, 'tools'))

import multiprocessing
import baselines.common.tf_util as U
import numpy as np
import tensorflow as tf
import argparse
import gym
import baselines
from baselines import logger
from baselines.common import mpi_config, set_global_seeds
useMPI=mpi_config.try_import_mpi()
if useMPI:
    from mpi4py import MPI
from time import time

from tools.utils import load_state, load_rewards, clear_state

from baselines.ppo1 import pposgd_simple
from baselines.ddpg import ddpg_run
from baselines.trpo_mpi import trpo_mpi

from envs.climb_envs import *
from envs.climb_envs.robot_povray_renderer import RobotPovrayRenderer
from envs.pybulletgym.envs import *
import pybullet

from algo_utils import argparser_func, act_func
from mlsh import MLSH


def pretrain():
    from tensorboardX import SummaryWriter
    writer = SummaryWriter()
    sess = tf.get_default_session()

    env = gym.make('WalkerEnv-v0')
    mlsh = MLSH('pi', env.observation_space, [None] * 3, [128, 64])
    saver = tf.train.Saver(mlsh.get_variables())
    batch_size = 64

    lr = tf.placeholder(name='lr', dtype=tf.float32)

    labels = tf.placeholder(name='labels', dtype=tf.int32, shape=None)
    loss = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=labels, logits=mlsh.logits, name='xent')
    loss = tf.reduce_mean(loss, name='loss')

    optimizer = tf.train.AdamOptimizer(learning_rate=lr)
    training_op = optimizer.minimize(loss, name='train_step')

    train_x = list()
    train_y = list()

    for i, file in enumerate(['WalkerEnv-v0', 'WCTransEnv-v0', 'ClimberEnv-v0']):
        npy = np.load(file + '.npy')
        npy_y = np.load(file + '_y' + '.npy')
        train_x.append(npy)
        # single_labels = np.ones((npy.shape[0],)) * i
        # if i == 1:
        #     single_labels = np.concatenate([
        #         np.ones((int(npy.shape[0] * 4 / 5),)) * 1,
        #         np.ones((int(npy.shape[0] / 5),)) * 2
        #     ])
        #     np.random.shuffle(single_labels)
        train_y.append(npy_y)

    def T(arr):
        arr = np.stack(arr, axis=0)
        shape = list(arr.shape)
        t = list(range(len(arr.shape)))
        print(shape, t)
        t[0] = 1
        t[1] = 0
        arr = np.transpose(arr, t)
        arr = np.reshape(arr, [-1,] + shape[2:])
        return arr

    train_x = T(train_x)
    train_y = T(train_y)

    x_ph = tf.placeholder(tf.float32, [None, 57])
    y_ph = tf.placeholder(tf.int32, None)

    dataset = tf.data.Dataset.from_tensor_slices((x_ph, y_ph))
    dataset = dataset.shuffle(buffer_size=1000000).batch(batch_size).repeat()
    iterator = dataset.make_initializable_iterator()
    one_element = iterator.get_next()

    sess.run(iterator.initializer, feed_dict={x_ph: train_x, y_ph: train_y})
    U.initialize()

    lr_cum = 0.01
    step_cum = 0
    for epoch in range(1, 10):
        for step in range(len(train_y) // batch_size):
            step_cum += 1
            x_batch, y_batch = sess.run(one_element)
            loss_record, _, pred = sess.run([loss, training_op, mlsh.logits], feed_dict={mlsh.ob: x_batch, labels: y_batch, lr: lr_cum})
            acc = np.mean(np.argmax(pred, axis=1) == y_batch)
            writer.add_scalar('data/acc', acc, step_cum)
            writer.add_scalar('data/loss', loss_record, step_cum)
            writer.add_scalar('data/lr', lr_cum, step_cum)
            if step % 50 == 0:
                print('{}->{}/{}'.format(epoch, step, len(train_y) // batch_size), acc)
                # print('pred')
                # print(pred)
                # print('groundtruth')
                # print(y_batch)
            if step == 1000:
                saver.save(sess, 'mlsh.ckpt')
                sys.exit(0)
        if epoch % 3 == 0:
            lr_cum *= 0.1
    saver.save(sess, 'mlsh.ckpt')



def main():
    #tf configs
    ncpu = multiprocessing.cpu_count()
    if sys.platform == 'darwin': ncpu //= 2
    config = tf.ConfigProto(allow_soft_placement=True,
                            intra_op_parallelism_threads=ncpu,
                            inter_op_parallelism_threads=ncpu,
                            device_count={'GPU': 0})
    config.gpu_options.allow_growth = True
    tf.Session(config=config).__enter__()

    pretrain()


if __name__ == '__main__':
    main()
