import tensorflow as tf
import numpy as np
import gym
from collections import deque
import random
import time

import baselines.common.tf_util as U
from baselines.ppo1 import mlp_policy
from baselines.common.mpi_adam import MpiAdam
from baselines.common.mpi_moments import mpi_moments
from baselines.common import mpi_config, set_global_seeds, zipsame

useMPI = mpi_config.try_import_mpi()
if useMPI:
    from mpi4py import MPI

import os, sys

base_dir = os.path.dirname(os.path.abspath(__file__ + '/../'))
sys.path.insert(0, base_dir + '/envs/')
sys.path.insert(0, base_dir + '/tools/')
result_path = base_dir + '/results/'

from climb_envs import *
from utils import save_rewards, load_rewards


def adjust_lr_exp(lr_ori, cur_epoch, decay_epoch=200):
    if cur_epoch > 0 and cur_epoch % decay_epoch == 0 and (not useMPI or MPI.COMM_WORLD.Get_rank() == 0):
        print('Decaying `lr` to {}'.format(lr_ori / 3 ** (cur_epoch // decay_epoch)))
    return lr_ori / (3 ** (cur_epoch // decay_epoch))


def adjust_beta_lin(cur_epoch, decay_epoch=(100, 200), decay_value=(0.1, 0.0)):
    last_epoch, last_beta = 0, 1.0
    for epoch, beta in zip(decay_epoch, decay_value):
        if cur_epoch < epoch:
            if epoch == last_epoch:
                return last_beta
            else:
                return beta + (epoch - cur_epoch) * (last_beta - beta) / (epoch - last_epoch)
        else:
            last_epoch, last_beta = epoch, beta
    return last_beta


def learn(identifier, T, N,
          batch_size, dataset_size,
          expert_ids, expert_iters, expert_weights=None,
          expert_net_size=(128, 64), student_net_size=(128, 64),
          p=None, optim_epoch=10, optim_stepsize=1e-3, adam_epsilon=1e-5,
          render=False, save_interval=None, tb_logdir=None, seed=0,
          cont_iter=None, init_state_ids=None, init_state_iters=None):
    """
    ### dagger parameter ###
    :param p: decay rate of beta (expert weight)
    :param T: sample trajectory length
    :param N: total iteration num
    :param dataset_size: max capacity of dataset

    ### learning parameter ###
    :param batch_size: mini-batch size of sgd(adam)
    :param optim_epoch: same as 'pposgd_simple.py'
    :param optim_stepsize: same as 'pposgd_simple.py'
    :param adam_epsilon: same as 'pposgd_simple.py'

    ### other settings ###
    :param render: whether render while sampling
    :param save_interval: iteration interval between saving student models
    :param tb_logdir: Tensorboard logdir (enable Tensorboard if not None)
    """
    if useMPI:
        set_global_seeds(seed + MPI.COMM_WORLD.Get_rank())
    else:
        set_global_seeds(seed)

    if useMPI:
        T = (T + MPI.COMM_WORLD.Get_rank()) // MPI.COMM_WORLD.Get_size()

    assert len(expert_ids) == len(expert_iters)
    if expert_weights is not None:
        assert len(expert_weights) == len(expert_ids)
        for weight in expert_weights:
            assert weight >= 0
    else:
        expert_weights = [1] * len(expert_ids)

    if init_state_ids is not None or init_state_iters is not None:
        assert len(init_state_ids) == len(init_state_iters) == len(expert_ids)
        for init_state_id, init_state_iter in zip(init_state_ids, init_state_iters):
            if init_state_id != 'x':
                assert init_state_iter >= 0
    else:
        init_state_ids = ['x'] * len(expert_ids)
        init_state_iters = [None] * len(expert_ids)

    def exp_policy_fn(name, ob_space, ac_space):
        return mlp_policy.MlpPolicy(name=name, ob_space=ob_space, ac_space=ac_space,
                                    hid_layer_sizes=expert_net_size)

    def stu_policy_fn(name, ob_space, ac_space):
        return mlp_policy.MlpPolicy(name=name, ob_space=ob_space, ac_space=ac_space,
                                    hid_layer_sizes=student_net_size)

    # make expert envs
    exp_envs = []
    ob_space, ac_space = None, None
    for exp_id, exp_weight, init_state_id, init_state_iter in zip(expert_ids, expert_weights, init_state_ids, init_state_iters):
        exp_env = gym.make(exp_id)
        if ob_space is None:
            ob_space = exp_env.observation_space
            ac_space = exp_env.action_space
        else:
            assert ob_space == exp_env.observation_space and ac_space == exp_env.action_space
        if hasattr(exp_env.env, 'load_init_state') and init_state_id != 'x':
            exp_env.env.load_init_state(init_state_id, init_state_iter)
        exp_env.seed(seed)
        exp_envs.append(exp_env)
    n_envs = len(exp_envs)

    # build student & experts
    pi_stu = stu_policy_fn("pi", ob_space, ac_space)
    oldpi_stu = stu_policy_fn("oldpi", ob_space, ac_space)
    pi_exps = []
    for exp_id, exp_freq in zip(expert_ids, expert_weights):
        with tf.variable_scope(exp_id):
            pi_exp = exp_policy_fn("pi", ob_space, ac_space)
            pi_exps.append(pi_exp)

    assign_old_eq_new = U.function([], [], updates=[tf.assign(oldv, newv)
                                                    for (oldv, newv) in
                                                    zipsame(oldpi_stu.get_variables(), pi_stu.get_variables())])

    if render:
        if not useMPI or MPI.COMM_WORLD.Get_rank() == 0:
            for env in exp_envs:
                env.env.isRender = True

    # model mixture
    with tf.variable_scope('model_mix'):
        pi_acts = []
        beta = tf.placeholder(dtype=tf.float32, shape=())
        stochastic = tf.placeholder(dtype=tf.bool, shape=())
        observation = U.get_placeholder(name="ob", dtype=tf.float32, shape=[None] + list(ob_space.shape))
        for pi_exp in pi_exps:
            pd_exp, pd_stu = pi_exp.pd, pi_stu.pd
            mean = beta * pd_exp.mean + (1 - beta) * pd_stu.mean
            std = tf.sqrt((beta * pd_exp.std) ** 2 +
                          ((1 - beta) * pd_stu.std) ** 2 +
                          (beta * (1 - beta) * (pd_exp.mean - pd_stu.mean)) ** 2)
            action_sample = mean + std * tf.random_normal(tf.shape(mean))
            action = U.switch(stochastic, action_sample, mean)
            pi_act = U.function([stochastic, beta, observation], [action, mean, std])
            pi_acts.append(pi_act)

    # train student
    with tf.variable_scope('stu_train'):
        mean_label = tf.placeholder(dtype=tf.float32, shape=[None] + list(ac_space.shape))
        std_label = tf.placeholder(dtype=tf.float32, shape=[None] + list(ac_space.shape))
        loss_weight = tf.placeholder(dtype=tf.float32, shape=[None, 1])
        loss_mean = tf.reduce_sum(tf.square(pd_stu.mean - mean_label) * loss_weight, name='mean_sum')
        loss_std = tf.reduce_sum(tf.square(pd_stu.std - std_label) * loss_weight ** 2, name='std_sum')
        loss = loss_mean + loss_std

    var_list = pi_stu.get_trainable_variables()
    adam = MpiAdam(var_list, epsilon=adam_epsilon)
    inputs = [stochastic, beta, observation, mean_label, std_label, loss_weight]
    lossandgrad = U.function(inputs, [loss_mean, loss_std, U.flatgrad(loss, var_list)])

    # initialize & restore experts
    U.initialize()
    for exp_id, exp_iter in zip(expert_ids, expert_iters):
        exp_saver = tf.train.Saver(var_list=tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=exp_id))
        exp_saver.restore(tf.get_default_session(),
                          'results/saved_model/experts/' + exp_id + '_' + str(exp_iter) + '.ckpt')
    stu_saver = tf.train.Saver(var_list=tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='pi')
        + tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='oldpi'))
    reward_list = []
    if cont_iter is not None:
        stu_saver.restore(tf.get_default_session(),
                          'results/saved_model/model_data/' + identifier + '_' + str(cont_iter) + '.ckpt')
        reward_list = load_rewards(identifier, cont_iter)
    adam.sync()

    if tb_logdir is not None:
        tf.summary.scalar('loss', loss)
        tf.summary.scalar('loss_mean', loss_mean)
        tf.summary.scalar('loss_std', loss_std)
        writer = tf.summary.FileWriter(tb_logdir, tf.get_default_graph())

    # dataset
    D = deque(maxlen=dataset_size)
    obs = deque(maxlen=T)

    start_iter = 0 if cont_iter is None else cont_iter

    for i in range(start_iter, N):
        t_start = time.time()

        cur_lr = adjust_lr_exp(optim_stepsize, i, 200)
        cur_beta = adjust_beta_lin(i, (100, 200), (0.1, 0.0)) if p is None else p ** i

        env = exp_envs[i % n_envs]
        pi_act = pi_acts[i % n_envs]
        pi_exp = pi_exps[i % n_envs]
        exp_weight = expert_weights[i % n_envs]

        ob = env.reset()
        D_temp = []

        # sample T-step trajectories using pi_i
        for _ in range(T):
            ac, _, _ = pi_act(False, cur_beta, ob)

            # get dataset D of visited states by pi_i and actions given by expert
            ac_exp, mean_exp, std_exp = pi_exp.act_pd(False, ob)
            # aggregate datasets
            D_temp.append([ob, mean_exp, std_exp, exp_weight])
            obs.append(ob)
            ob, rew, done, _ = env.step(ac[0])
            if render:
                if not useMPI or MPI.COMM_WORLD.Get_rank() == 0:
                    env.render(mode='human')
            if done:
                ob = env.reset()

        D_traj_gather = MPI.COMM_WORLD.allgather(D_temp) if useMPI else [D_temp]
        D_traj_all = []
        for D_traj in D_traj_gather:
            D_traj_all = D_traj_all + D_traj

        D.extend(D_traj_all)

        assign_old_eq_new()

        if hasattr(pi_stu, "ob_rms"):
            # print("updating rms with len(obs)={}/{}".format(len(obs), T))
            pi_stu.ob_rms.update(np.array(obs))

        # train classifier pi_i+1 on D
        random.shuffle(D)
        real_batch_size = min(batch_size, len(D))
        losses_mean, losses_std = [], []

        for epoch in range(optim_epoch):
            for j in range(0, len(D), real_batch_size):
                data = list(D)[j:j + real_batch_size]
                ob_batch, mean_batch, std_batch, weight_batch = np.array(data).transpose()
                ob_batch, mean_batch, std_batch, weight_batch = np.stack(ob_batch), np.stack(mean_batch), np.stack(std_batch), np.stack(weight_batch)
                input_batch = [False, cur_beta, ob_batch, mean_batch, std_batch, weight_batch]

                newloss_mean, newloss_std, g = lossandgrad(*input_batch)
                adam.update(g, cur_lr)
                losses_mean.append(newloss_mean)
                losses_std.append(newloss_std)

        meanloss_mean, _, _ = mpi_moments(losses_mean, axis=0)
        meanloss_std, _, _ = mpi_moments(losses_std, axis=0)
        meanloss = meanloss_mean + meanloss_std
        if not useMPI or MPI.COMM_WORLD.Get_rank() == 0:
            reward_list.append(meanloss)
            print('''Iteration: {:5d} '''
                  '''Loss: {:9.2f} '''
                  '''Time: {:6.2f} '''
                  '''Dataset: {:6d} '''
                  '''lr: {:8.6f} '''
                  '''beta: {:8.6f} '''.format(i, meanloss, time.time() - t_start, len(D), cur_lr, cur_beta))

            if tb_logdir is not None:
                merged = tf.summary.merge_all()
                summary = tf.get_default_session().run(merged,
                                                       feed_dict={loss: meanloss,
                                                                  loss_mean: meanloss_mean,
                                                                  loss_std: meanloss_std})
                writer.add_summary(summary, i)

            if save_interval is not None and (i + 1) % save_interval == 0:
                fname = result_path + 'saved_model/model_data/' + identifier + '_' + str(i + 1) + '.ckpt'
                os.makedirs(os.path.dirname(fname), exist_ok=True)
                stu_saver.save(tf.get_default_session(), fname)
                save_rewards(reward_list, identifier, i + 1)
                print('Model and loss saved')
