from argparse import ArgumentParser
import os
import matplotlib.pyplot as plt
import numpy as np
import json

base_dir = os.path.dirname(os.path.abspath(__file__)) + '/../'
result_path = base_dir + 'results/saved_model/'


def plot_rewards(reward_list, title, fit=False, order=6, show=False):
    x = np.arange(len(reward_list))
    plt.figure('Training Result')

    if fit:
        fit_curve = np.polyfit(x, reward_list, order)
        fit_curve = np.polyval(fit_curve, x)
        plt.plot(x, fit_curve, lw=2, label=title, color='r')

    plt.plot(x, reward_list, color='r', alpha=0.5)

    plt.xlabel('Iteration Number')
    plt.ylabel('Episode Reward')
    plt.grid(True)

    save_path = result_path + 'reward_images/'
    os.makedirs(save_path, exist_ok=True)
    if show:
        plt.show()
    else:
        plt.savefig(save_path + title + ".png")
    plt.close()

def main():

    parser = ArgumentParser(description='Plot.')
    parser.add_argument('--id', type=str)
    parser.add_argument('--iter', type=str, default='final')
    parser.add_argument('--order', type=int, default=6)
    parser.add_argument('--fit', default=False, action='store_true')
    parser.add_argument('--show', default=False, action='store_true')
    args = parser.parse_args()

    reward_json_path = result_path + 'reward_jsons/' + args.id + '_' + args.iter + '.json'
    with open(reward_json_path, 'r') as file:
        reward_list = list(json.load(file))
    
    plot_rewards(reward_list, args.id, args.fit, args.order, args.show)


if __name__ == '__main__':
    main()
