import baselines.common.tf_util as U
import tensorflow as tf
from baselines.common.distributions import DiagGaussianPdType, CategoricalPdType

class MLSH(object):
    recurrent = False
    def __init__(self, name, ob_space, subpolicies, hid_layer_sizes, ob_ph=None):
        assert type(subpolicies) == list
        self.subpolicies = subpolicies
        self.len_subpolicy = len(self.subpolicies)
        self.scope = 'mlsh_' + name

        with tf.variable_scope(self.scope):
            if ob_ph is None:
                ob = U.get_placeholder(name="ob", dtype=tf.float32, shape=[None] + list(ob_space.shape))
            else:
                ob = ob_ph
            last_out = ob

            with tf.variable_scope('vf'):
                for i, size in enumerate(hid_layer_sizes):
                    last_out = tf.nn.relu(tf.layers.dense(last_out, size, name='fc%i' % (i + 1),
                        kernel_initializer=U.normc_initializer(1.0)))
                self.vpred = tf.layers.dense(last_out, 1, name='final', kernel_initializer=U.normc_initializer(1.0))[:, 0]

            with tf.variable_scope('pol'):
                for i, size in enumerate(hid_layer_sizes):
                    last_out = tf.nn.relu(tf.layers.dense(last_out, size, name='fc%i' % (i + 1),
                        kernel_initializer=U.normc_initializer(1.0)))
                mean = tf.layers.dense(last_out, self.len_subpolicy, name='final',
                    kernel_initializer=U.normc_initializer(1.0))

        self.logits = mean
        self.ob = ob

        self.pdtype = pdtype = CategoricalPdType(self.len_subpolicy)
        self.pd = pdtype.pdfromflat(mean)

        stochastic = tf.placeholder(dtype=tf.bool, shape=())
        ac = U.switch(stochastic, self.pd.sample(), self.pd.mode())
        self._act = U.function([stochastic, ob], [ac, self.vpred])
            
    def act(self, stochastic, ob):
        ac_master, vpred = self._act(stochastic, ob[None])
        ac_sub, _ = self.subpolicies[int(ac_master)].act(False, ob)
        return ac_master[0], ac_sub, vpred[0]

    def get_variables(self):
        return tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.scope)
    def get_trainable_variables(self):
        return tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, self.scope)
    def get_initial_state(self):
        return []


# class MLSH(object):
#     recurrent = False
#
#     def __init__(self, name, ob_space, subpolicies, hid_layer_sizes, ob_ph=None):
#         assert type(subpolicies) == list
#         self.subpolicies = subpolicies
#         self.len_subpolicy = len(self.subpolicies)
#         self.scope = 'mlsh_' + name
#
#         with tf.variable_scope(self.scope):
#             if ob_ph is None:
#                 ob = U.get_placeholder(name="ob", dtype=tf.float32, shape=[None] + list(ob_space.shape))
#             else:
#                 ob = ob_ph
#             last_out = ob
#
#             with tf.variable_scope('vf'):
#                 for i, size in enumerate(hid_layer_sizes):
#                     last_out = tf.nn.relu(tf.layers.dense(last_out, size, name='fc%i' % (i + 1),
#                                                           kernel_initializer=U.normc_initializer(1.0)))
#                 self.vpred = tf.layers.dense(last_out, 1, name='final', kernel_initializer=U.normc_initializer(1.0))[:,
#                              0]
#
#             with tf.variable_scope('pol'):
#                 for i, size in enumerate(hid_layer_sizes):
#                     last_out = tf.nn.relu(tf.layers.dense(last_out, size, name='fc%i' % (i + 1),
#                                                           kernel_initializer=U.normc_initializer(1.0)))
#                 mean = tf.layers.dense(last_out, self.len_subpolicy, name='final',
#                                        kernel_initializer=U.normc_initializer(1.0))
#                 logstd = tf.get_variable(name="logstd", shape=[1, self.len_subpolicy],
#                                          initializer=tf.zeros_initializer())
#                 pdparam = tf.concat([mean, mean * 0.0 + logstd], axis=1)
#
#         self.logits = mean
#         self.ob = ob
#
#         self.pdtype = pdtype = DiagGaussianPdType(self.len_subpolicy)
#         self.pd = pdtype.pdfromflat(pdparam)
#
#         stochastic = tf.placeholder(dtype=tf.bool, shape=())
#         ac = U.switch(stochastic, self.pd.sample(), self.pd.mode())
#         # self._act = U.function([stochastic, ob], [ac, self.vpred])
#
#         prob = tf.expand_dims(tf.nn.softmax(ac), -1)
#         acs = [policy.mean for policy in subpolicies]
#         acs = tf.stack(acs, axis=1)
#         final_act = tf.reduce_sum(prob * acs, axis=1)
#         self._act = U.function([stochastic, ob], [ac, final_act, self.vpred])
#
#     def act(self, stochastic, ob):
#         ac_master, final_act, vpred = self._act(stochastic, ob[None])
#         # ac_sub, _ = self.subpolicies[int(ac_master)].act(False, ob)
#         return ac_master[0], final_act[0], vpred[0]
#         # prob = tf.nn.softmax(self.logits)
#         # ac_0, _ = self.subpolicies[0].act(False, ob)
#         # ac_1, _ = self.subpolicies[1].act(False, ob)
#         # ac = prob[0][0] * ac_0 + prob[0][1] * ac_1
#         # ac_sub, prob_print = tf.get_default_session().run([ac, prob], feed_dict={self.ob: ob[None]})
#         # return 0, ac_sub, 0
#
#     def get_variables(self):
#         return tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.scope)
#
#     def get_trainable_variables(self):
#         return tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, self.scope)
#
#     def get_initial_state(self):
#         return []
