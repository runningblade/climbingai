import os,sys
from shutil import rmtree
base_dir = os.path.dirname(os.path.abspath(__file__)) + '/../'
sys.path.append(base_dir)
sys.path.append(base_dir + 'simulator/build/')
sys.path.insert(0, os.path.join(base_dir, 'envs'))
sys.path.insert(0, os.path.join(base_dir, 'tools'))
os.environ['OMP_NUM_THREADS'] = '1'

import multiprocessing
import tensorflow as tf
import argparse
import gym
import baselines
from baselines import logger
from baselines.common import mpi_config, set_global_seeds
useMPI=mpi_config.try_import_mpi()
if useMPI:
    from mpi4py import MPI
from time import time

from tools.utils import load_state, load_rewards, clear_state, load_next_state

from baselines.ppo1 import pposgd_simple
from baselines.trpo_mpi import trpo_mpi

from envs.climb_envs import *
from envs.climb_envs.robot_povray_renderer import RobotPovrayRenderer
from envs.pybulletgym.envs import *
import pybullet

from algo_utils import argparser_func, act_func

algo_backbones = {
    'ppo': pposgd_simple.learn,
    'trpo': trpo_mpi.learn,
}

def train(args):

    identifier = args['id']
    env = gym.make(identifier)
    env.env.set_params(seed=args['seed'],
                    exp_name=args['exp_name'],
                    next_env_iter=args['next_env_iter'],
                    curriculum=args['curriculum'],
                    init_state_id=args['init_state_id'],
                    init_state_iter=args['init_state_iter'])

    identifier += '_' + args['exp_name'] if args['exp_name'] is not None else ''

    if args['clear'] and (not useMPI or MPI.COMM_WORLD.Get_rank()==0):
        print('Clearing: '+identifier)
        clear_state(identifier)

    if args['cont']:
        assert args['iter'] is not None
        reward_list = load_rewards(identifier, args['iter'])
    else:
        reward_list = []

    if useMPI:
        set_global_seeds(args['seed'] + MPI.COMM_WORLD.Get_rank())
    else:
        set_global_seeds(args['seed'])

    backbone = args['backbone']
    args['identifier'] = identifier
    train_args = args.copy()
    for key in ['id', 'seed', 'clear', 'backbone', 'save_final_state', 'init_state_id', 'init_state_iter', 'exp_name', 'gen_dataset']:
        del train_args[key]

    pi = algo_backbones[backbone](env=env, reward_list=reward_list, **train_args)
    
    env.close()

    return pi


def test(args, r_args):

    identifier = args['id']
    mlsh = args['mlsh']
    env = gym.make(identifier)
    env.env.set_params(seed=args['seed'],
                    exp_name=args['exp_name'],
                    next_env_iter=args['next_env_iter'],
                    curriculum=args['curriculum'],
                    init_state_id=args['init_state_id'],
                    init_state_iter=args['init_state_iter'])

    identifier += '_' + args['exp_name'] if args['exp_name'] is not None else ''
    backbone = args['backbone']

    pov_path = os.path.join(base_dir, 'results', 'pov', identifier)
    os.makedirs(pov_path, exist_ok=True)

    args['next_env_iter'] = None

    pi = train(args)
    load_state(identifier, args['iter'])
        
    if isinstance(env,gym.Wrapper):
        env = env.env
    if r_args['record'] != '':
        env._render_width = int(r_args['record'].split(',')[0])
        env._render_height = int(r_args['record'].split(',')[1])
        env = gym.wrappers.Monitor(env, pov_path, force=True)
    else: 
        env = gym.wrappers.TimeLimit(env)
    if (not useMPI or MPI.COMM_WORLD.Get_size() == 1):
        env.env.isRender = True

    env.env._cam_yaw = r_args['yaw_init']
    env.env._cam_pitch = r_args['pitch_init']
    env.env._cam_dist = r_args['dist_init']

    if hasattr(env.env, 'geom_path') and os.path.exists(env.env.geom_path):
        rmtree(env.env.geom_path)

    observation = env.reset()

    if r_args['pov']:
        env.env.povrayRenderer = RobotPovrayRenderer(env.env.robot, pov_path)

    step = 0
    iter = MPI.COMM_WORLD.Get_rank() if useMPI else 0
    round = 0
    t1 = time()
    cum_rew = 0
    while True:
        t2 = time()
        if (t2 - t1) < 1 / float(r_args['FPS']):
            continue
        action = act_func[backbone](pi, observation, mlsh)
        observation, reward, done, info = env.step(action)
        cum_rew += reward
        step += 1
        if (not useMPI or MPI.COMM_WORLD.Get_size() == 1):
            print('step', step)
            env.render(mode='rgb_array' if r_args['record'] else 'human')
        if args['save_final_state'] and hasattr(env.env, 'save_final_state') and 'final' in info and info['final']:
            env.env.save_final_state(identifier, iter)
            iter += MPI.COMM_WORLD.Get_size() if useMPI else 1
            if useMPI:
                MPI.COMM_WORLD.Barrier()
            done = True
        t1 = time()
        if done or step > 1000:
            step = 0
            print('round', round, 'finished')
            print('cumulative reward', cum_rew)
            round += 1
            cum_rew = 0
            observation = env.reset()

    env.close()


def sequence_test(args, r_args):
    env_ids = ['RandomWalkerEnv-v0', 'RandomClimberEnv-v0', 'RandomStanderEnv-v0']
    env_iters = args['seq_iters']
    next_env_iters = env_iters[1:] + [env_iters[0]]

    envs = []
    next_pis = []
    for env_id, next_env_iter in zip(env_ids, next_env_iters):
        env = gym.make(env_id)

        env.env.seed(args['seed'])
        env.env.isRender = True
        env.env._cam_yaw = r_args['yaw_init']
        env.env._cam_pitch = r_args['pitch_init']
        env.env._cam_dist = r_args['dist_init']
        if hasattr(env.env, 'geom_path') and os.path.exists(env.env.geom_path):
            rmtree(env.env.geom_path)

        next_pi = env.env.set_next_env(ob_prefix=env_id + '_')
        load_next_state(env.env.next_env_id, next_env_iter)
        env.env.next_env = None
        envs.append(env.env)
        next_pis.append(next_pi)

    pis = [next_pis[-1]] + next_pis[:-1]

    master_env = envs[0]
    observation = master_env.reset()

    curr_id = 0
    env = envs[curr_id]
    pi = pis[curr_id]

    step = 0
    max_steps = 500
    cum_rew = 0
    t1 = time()
    while True:
        t2 = time()
        if (t2 - t1) < 1 / float(r_args['FPS']):
            continue
        t1 = time()

        observation, reward, done, _ = master_env.step_with_other_env(observation, env, pi)
        cum_rew += reward
        step += 1
        print('step', step)
        master_env.render(mode='rgb_array' if r_args['record'] else 'human')

        if done:
            curr_id = (curr_id + 1) % 3
            env = envs[curr_id]
            pi = pis[curr_id]
            observation = master_env.reset_other_env(env)
        
        if step > max_steps:
            print('cumulative reward', cum_rew)
            step = 0
            cum_rew = 0
            observation = master_env.reset()
            curr_id = 0
            env = envs[curr_id]
            pi = pis[curr_id]


# NOTE: this function is not compatible with current code, will fix it later
def mlsh_test(args, r_args):
    identifier = 'End2EndEnv-v0'
    env = gym.make(identifier)
    env.env.seed(args['seed'])
    identifier += '_' + args['exp_name'] if args['exp_name'] is not None else ''

    backbone = args['backbone']

    pov_path = os.path.join(base_dir, 'results', 'pov', identifier)
    os.makedirs(pov_path, exist_ok=True)

    if args['init_state_id'] is not None and args['init_state_iter'] > 0:
        if hasattr(env.env, 'load_init_state'):
            env.env.load_init_state(args['init_state_id'], args['init_state_iter'])

    pi = train(args)
    # load_state(identifier, args['iter'])
    mlsh_saver = tf.train.Saver(var_list=tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='mlsh'))
    mlsh_saver.restore(tf.get_default_session(), 'results/saved_model/mlsh/mlsh.ckpt')
    for exp_id, exp_iter in zip(args['expert_ids'], args['expert_iters']):
        exp_saver = tf.train.Saver(var_list=tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='sub_' + exp_id))
        exp_saver.restore(tf.get_default_session(),
                          'results/saved_model/experts/' + exp_id + '_' + str(exp_iter) + '.ckpt')

    if isinstance(env, gym.Wrapper):
        env = env.env
    if r_args['record'] != '':
        env._render_width = int(r_args['record'].split(',')[0])
        env._render_height = int(r_args['record'].split(',')[1])
        env = gym.wrappers.Monitor(env, pov_path, force=True)
    else:
        env = gym.wrappers.TimeLimit(env)
    env.env.isRender = True

    env.env._cam_yaw = r_args['yaw_init']
    env.env._cam_pitch = r_args['pitch_init']
    env.env._cam_dist = r_args['dist_init']

    if hasattr(env.env, 'geom_path') and os.path.exists(env.env.geom_path):
        rmtree(env.env.geom_path)

    observation = env.reset()

    if r_args['pov']:
        env.env.povrayRenderer = RobotPovrayRenderer(env.env.robot, pov_path)

    step = 0
    iter = 0
    t1 = time()
    while True:
        t2 = time()
        if (t2 - t1) < 1 / float(r_args['FPS']):
            continue
        action = act_func[backbone](pi, observation, True)
        observation, reward, done, info = env.step(action)
        step += 1
        print('step', step)
        env.render(mode='rgb_array' if r_args['record'] else 'human')
        if args['save_final_state'] and hasattr(env.env, 'save_final_state') and 'final' in info and info['final']:
            env.env.save_final_state(identifier, iter)
            iter += 1
            done = True
        t1 = time()
        if done or step > 1000:
            step = 0
            observation = env.reset()

    env.close()

# NOTE: this function is not compatible with current code, will fix it later
def gen_dataset(args):
    import numpy as np

    identifier = args['id']
    env = gym.make(identifier)
    env.env.seed(args['seed'])
    identifier += '_' + args['exp_name'] if args['exp_name'] is not None else ''

    backbone = args['backbone']

    pi = train(args)
    load_state(identifier, args['iter'])

    if isinstance(env, gym.Wrapper):
        env = env.env
    env = gym.wrappers.TimeLimit(env)
    observation = env.reset()

    bank = np.array([observation for _ in range(args['gen_dataset_step'])])
    labels = np.empty((args['gen_dataset_step'],))

    step = 0
    in_step = 0
    max_step = args['gen_dataset_step']
    while step < max_step:
        label = 1
        if in_step > 30:
            label = 2
        bank[step] = observation
        labels[step] = label
        action = act_func[backbone](pi, observation)
        observation, reward, done, info = env.step(action)
        step += 1
        in_step += 1
        if step % 100 == 0:
            print('step', step)
        if in_step >= 170:
            in_step = 0
            done = True
        if done:
            in_step = 0
            observation = env.reset()

    np.save(identifier, bank)
    np.save(identifier + '_y', labels)

    env.close()


def main():
    logger.configure()
    parser = argparse.ArgumentParser(description='ClimbingAI')
    parser.add_argument('--backbone', type=str, default='ppo')
    parser.add_argument('--id', type=str)
    parser.add_argument('--exp_name', type=str, default=None)
    parser.add_argument('--step', type=int, default=1e9)
    parser.add_argument('--gen_dataset_step', type=int, default=1e5)
    parser.add_argument('--save_interval', type=int, default=50)
    parser.add_argument('--save_result', default=False, action='store_true')
    parser.add_argument('--seed', type=int, default=0)
    parser.add_argument('--cont', default=False, action='store_true')
    parser.add_argument('--play', default=False, action='store_true')
    parser.add_argument('--clear', default=False, action='store_true')
    parser.add_argument('--iter', type=str, default='final')
    parser.add_argument('--action_repeat', type=int, default=1)
    parser.add_argument('--save_final_state', default=False, action='store_true')
    parser.add_argument('--init_state_id', type=str, default=None)
    parser.add_argument('--init_state_iter', type=int, default=-1)
    parser.add_argument('--fix_policy', default=False, action='store_true')
    parser.add_argument('--mlsh', default=False, action='store_true')
    parser.add_argument('--seq_iters', type=str, nargs='+', default=None)
    parser.add_argument('--expert_ids', type=str, nargs='+', default=None)
    parser.add_argument('--expert_iters', type=str, nargs='+', default=None)
    parser.add_argument('--next_env_iter', type=str, default=None)
    parser.add_argument('--gen_dataset', default=False, action='store_true')
    parser.add_argument('--curriculum', type=int, default=0)
    # render
    parser.add_argument('--pov', default=False, action='store_true')
    parser.add_argument('--record', type=str, default='')
    parser.add_argument('--yaw_step', type=int, default=1)
    parser.add_argument('--yaw_init', type=int, default=0)
    parser.add_argument('--pitch_step', type=int, default=1)
    parser.add_argument('--pitch_init', type=int, default=0)
    parser.add_argument('--dist_step', type=float, default=0.1)
    parser.add_argument('--dist_init', type=float, default=5)
    parser.add_argument('--FPS', type=int, default=60)
    g_args = parser.parse_known_args()[0]

    g_args.backbone = str.lower(g_args.backbone)
    backbone = g_args.backbone
    assert backbone in ['ppo', 'trpo']

    args = argparser_func[backbone](parser)

    if args['save_interval'] <= 0:
        args['save_interval'] = None
        args['save_result'] = False
    else:
        args['save_result'] = True

    #tf configs
    ncpu = multiprocessing.cpu_count()
    if sys.platform == 'darwin': ncpu //= 2
    config = tf.ConfigProto(allow_soft_placement=True,
                            intra_op_parallelism_threads=ncpu,
                            inter_op_parallelism_threads=2,
                            device_count={'GPU': 0})
    config.gpu_options.allow_growth = True
    tf.Session(config=config).__enter__()

    if not useMPI or MPI.COMM_WORLD.Get_rank() == 0:
        print('********** Configuration ************')
        for key, val in args.items():
            print(key, ':', val)
        print('*************************************')

    render_args = {}
    render_arg_names = ['pov', 'record', 'yaw_step', 'yaw_init', 'pitch_step', 'pitch_init', 'dist_step', 'dist_init', 'FPS']
    for name in render_arg_names:
        render_args[name] = args[name]
        del args[name]

    if args['seq_iters'] is not None:
        sequence_test(args, render_args)
    elif not args['play']:
        train(args)
    # elif args['mlsh']:
    #     mlsh_test(args, render_args)
    elif args['gen_dataset']:
        gen_dataset(args)
    else:
        test(args, render_args)


if __name__ == '__main__':
    main()
