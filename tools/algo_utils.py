import numpy as np
import tensorflow as tf
from baselines.ppo1 import mlp_policy
from baselines.common.misc_util import boolean_flag
from baselines.a2c.utils import fc
import mlsh


def ppo_parse_args(parser):
    parser.add_argument('--hid_layer_sizes', type=int, nargs='+', default=(128, 64))
    parser.add_argument('--max_timesteps', type=int, default=1e9)
    parser.add_argument('--timesteps_per_actorbatch', type=int, default=1000)
    parser.add_argument('--clip_param', type=float, default=0.1)
    parser.add_argument('--entcoeff', type=float, default=0.0)
    parser.add_argument('--optim_epochs', type=int, default=10)
    parser.add_argument('--optim_stepsize', type=float, default=3e-4)
    parser.add_argument('--optim_batchsize', type=int, default=100)
    parser.add_argument('--gamma', type=float, default=0.99)
    parser.add_argument('--lam', type=float, default=0.95)
    parser.add_argument('--schedule', type=str, default='linear')
    args = parser.parse_args()

    if args.mlsh:
        def policy_fn(name, ob_space, ac_space):
            subpolicies = [mlp_policy.MlpPolicy(name='sub_' + expert_id + '/' + name, ob_space=ob_space, ac_space=ac_space,
                hid_layer_sizes=args.hid_layer_sizes) for expert_id in args.expert_ids]
            return mlsh.MLSH(name=name, ob_space=ob_space, subpolicies=subpolicies, hid_layer_sizes=args.hid_layer_sizes)
        
    else:
        def policy_fn(name, ob_space, ac_space):
            return mlp_policy.MlpPolicy(name=name, ob_space=ob_space, ac_space=ac_space,
                hid_layer_sizes=args.hid_layer_sizes)

    dict_args = vars(args)
    dict_args['policy_fn'] = policy_fn
    dict_args['max_timesteps'] = args.step if not args.play else 1
    for key in ['step']:
        del dict_args[key]

    return dict_args


def trpo_parse_args(parser):
    parser.add_argument('--timesteps_per_batch', type=int, default=1000)
    parser.add_argument('--hid_layer_sizes', type=int, nargs='+', default=(128, 64))
    args = parser.parse_args()

    def mlp(hid_layer_sizes, activation=tf.nn.relu):
        def network_fn(X):
            h = tf.layers.flatten(X)
            for i, sz in enumerate(hid_layer_sizes):
                h = activation(fc(h, 'mlp_fc{}'.format(i), nh=sz, init_scale=np.sqrt(2)))
            return h, None
        return network_fn

    dict_args = vars(args)
    dict_args['network'] = mlp(args.hid_layer_sizes)
    dict_args['total_timesteps'] = args.step if not args.play else 1
    for key in ['hid_layer_sizes', 'step']:
        del dict_args[key]

    return dict_args


argparser_func = {
    'ppo': ppo_parse_args,
    'trpo': trpo_parse_args,
}


def ppo_act_func(pi, ob, mlsh=False):
    if mlsh:
        act = pi.act(False, ob)
        return act[1]
    else:
        return pi.act(False, ob)[0]

def trpo_act_func(pi, ob):
    return pi.step(ob, stochastic=False)[0][0]


act_func = {
    'ppo': ppo_act_func,
    'trpo': trpo_act_func,
}