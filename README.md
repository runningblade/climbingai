## State-of-the-art Results

### Climber

Environment: ClimberCurriculumEnv-v0

Parameter:

    np 32
    (+default settings)
Iteration: 3000

### Walker

Environment: WalkerEnv-v0

Parameter:

```
np 32
(+default settings)
```

Iteration: 1500

### Dagger: Climber + Walker

Parameter:

```
np 4
expert_ids ClimberEnv-v0 WalkerEnv-v0
expert_iters 3000 2700
T 5000
p 0.9
optim_stepsize 3e-3
optim_epoch 20
student_net_size 256 128 64
(+default settings)
```