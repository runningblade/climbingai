#$1:cont_iter(optional), $2:next_iter(optional)
cont_args=""
if [ $# -ge 1 ]; then
    cont_args="--cont --iter $1"
fi
next_args=""
if [ $# -ge 2 ]; then
    next_args="--next_env_iter $2"
fi
mpirun -np 16 python tools/run.py --id RandomWalkerEnv-v0 $cont_args $next_args
