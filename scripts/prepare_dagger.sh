#$1:id,$2:iter
export MODEL_PATH=results/saved_model
export EXP_PATH=$MODEL_PATH/experts
mkdir -p $EXP_PATH
cp $MODEL_PATH/model_data/$1_$2.ckpt.* $EXP_PATH/
python3 tools/rename_tfvar.py --checkpoint_dir $EXP_PATH/$1_$2.ckpt --add_prefix $1/
