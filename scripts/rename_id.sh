#$1:iter, $2:old_name, $3:new_name
cp results/saved_model/model_data/$2_$1.ckpt.data-00000-of-00001 results/saved_model/model_data/$3_$1.ckpt.data-00000-of-00001
cp results/saved_model/model_data/$2_$1.ckpt.index results/saved_model/model_data/$3_$1.ckpt.index
cp results/saved_model/model_data/$2_$1.ckpt.meta results/saved_model/model_data/$3_$1.ckpt.meta
