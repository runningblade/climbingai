#$1:init_state_iter, $2:cont_iter(optional), $3:next_iter(optional)
cont_args=""
if [ $# -ge 2 ]; then
    cont_args="--cont --iter $2"
fi
next_args=""
if [ $# -ge 3 ]; then
    next_args="--next_env_iter $3"
fi
mpirun -np 16 python tools/run.py --id RandomStanderEnv-v0 --init_state_id RandomClimberEnv-v0 --init_state_iter $1 $cont_args $next_args
