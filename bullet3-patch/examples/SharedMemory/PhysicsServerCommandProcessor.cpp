	//insert these lines into this function:
	//int PhysicsServerCommandProcessor::extractCollisionShapes(const btCollisionShape* colShape, const btTransform& transform, b3CollisionShapeData* collisionShapeBuffer, int maxCollisionShapes)
	case MULTI_SPHERE_SHAPE_PROXYTYPE:
	{
		btMultiSphereShape* sphere = (btMultiSphereShape*)colShape;
		int nr = sphere->getSphereCount();
		if (nr > maxCollisionShapes)
		  nr = maxCollisionShapes;
		for (int i = 0; i < nr; i++)
		{
		      btTransform childTrans = transform*btTransform(btQuaternion(0,0,0,1),sphere->getSpherePosition(i));
		      collisionShapeBuffer[i].m_collisionGeometryType = GEOM_SPHERE;
		      collisionShapeBuffer[i].m_localCollisionFrame[0] = childTrans.getOrigin()[0];
		      collisionShapeBuffer[i].m_localCollisionFrame[1] = childTrans.getOrigin()[1];
		      collisionShapeBuffer[i].m_localCollisionFrame[2] = childTrans.getOrigin()[2];
		      collisionShapeBuffer[i].m_localCollisionFrame[3] = childTrans.getRotation()[0];
		      collisionShapeBuffer[i].m_localCollisionFrame[4] = childTrans.getRotation()[1];
		      collisionShapeBuffer[i].m_localCollisionFrame[5] = childTrans.getRotation()[2];
		      collisionShapeBuffer[i].m_localCollisionFrame[6] = childTrans.getRotation()[3];
		      collisionShapeBuffer[i].m_dimensions[0] = sphere->getSphereRadius(i);
		      collisionShapeBuffer[i].m_dimensions[1] = sphere->getSphereRadius(i);
		      collisionShapeBuffer[i].m_dimensions[2] = sphere->getSphereRadius(i);
		      numConverted++;
		}
		break;
	}
