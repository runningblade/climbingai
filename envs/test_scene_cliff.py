import pybullet
from pybullet_envs.bullet import bullet_client
from climb_envs.scene_cliff import *

#create
tfloor=-10
twall=20
bullet_client = bullet_client.BulletClient()
cliff_scene = CliffScene(bullet_client, gravity=9.8, timestep=0.0165/4, frame_skip=4, theta_floor=tfloor, theta_wall=twall)
cliff_scene.episode_restart(bullet_client)
cliff_scene.test_scene_feature(100)