import pybullet
import numpy as np
from pybulletgym.envs.roboschool.robot_bases import MJCFBasedRobot
from pybulletgym.envs.roboschool.robot_locomotors import WalkerBase, Humanoid
import os
    
class RandomHumanoidClimber(Humanoid):
    #grasp
    foot_list = ["right_foot", "left_foot", "right_lower_arm", "left_lower_arm"]
    lower_foot_idx = slice(0, 2)
    upper_foot_idx = slice(2, 4)
    fixed = [False, False]
    foot_rel_pos = [np.array([  0,   0,0.1]).astype(np.float),
                    np.array([  0,   0,0.1]).astype(np.float),
                    np.array([.18, .18,.18]).astype(np.float),
                    np.array([.18,-.18,.18]).astype(np.float)]
    foot_rel_rad = [0.075, 0.075, 0.04, 0.04]
    
    #render
    graspable_object_color = (153.0/255.0, 51.0/255.0, 51.0/255.0, 1.0)
    non_graspable_object_color = (51.0/255.0, 153.0/255.0, 255.0/255.0, 1.0)
    non_graspable_color = (153.0/255.0, 102.0/255.0, 0.0, 1.0)
    graspable_color = (1.0, 0.0, 0.0, 1.0)
    in_grasp_color = (0.0, 1.0, 0.0, 1.0)
    
    def __init__(self, random_yaw = False, random_lean=False, power_scale=0, reset_ref=False, obs_dim=51):
        WalkerBase.__init__(self, power=0.41)
        MJCFBasedRobot.__init__(self, 'humanoid_symmetric.xml', 'torso', action_dim=17+4, obs_dim=obs_dim)
        # 17 joints, 4 of them important for walking (hip, knee), others may as well be turned off, 17/4 = 4.25
        self.random_yaw = random_yaw
        self.random_lean = random_lean
        self.ingrasp = [-1.0]*4

        self.power_scale = power_scale
        self.reset_ref = reset_ref
        self.init_state_info = None

    def reset(self, bullet_client):

        full_path = os.path.join(os.path.dirname(__file__), "..", "pybulletgym", "envs", "assets", "mjcf", self.model_xml)

        self._p = bullet_client
        self.ordered_joints = []
        if self.self_collision:
            self.objects = self._p.loadMJCF(full_path, flags=pybullet.URDF_USE_SELF_COLLISION|pybullet.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS)
        else:
            self.objects = self._p.loadMJCF(full_path)
        self.parts, self.jdict, self.ordered_joints, self.robot_body = self.addToScene(self._p, self.objects)
        self.robot_specific_reset(self._p)

        s = self.calc_state()  # optimization: calc_state() can calculate something in self.* for calc_potential() to use
        
        self.ingrasp = [-1.0]*4
        if hasattr(self,'grasp_info'):
            for _,v in self.grasp_info.items():
                v.graspid = -1
                v.unfix()
                v.graspable = False
                v.update_color(self)
        else:
            self.grasp_info = {}
            for k in range(len(self.foot_list)):
                self.grasp_info[self.foot_list[k]] = GraspInfo(
                    self, 
                    part=self.parts[self.foot_list[k]], 
                    rel_pos=self.foot_rel_pos[k],
                    rel_rad=self.foot_rel_rad[k]*1.1)
        
        return s

    def calc_potential(self):
        return 0

    def calc_state(self):
        j = np.array([j.current_relative_position() for j in self.ordered_joints], dtype=np.float32).flatten()
        # even elements [0::2] position, scaled to -1..+1 between limits
        # odd elements  [1::2] angular speed, scaled to show -1..+1
        self.joint_speeds = j[1::2]
        self.joints_at_limit = np.count_nonzero(np.abs(j[0::2]) > 0.99)

        body_pose = self.robot_body.pose()
        # parts_xyz = np.array([p.pose().xyz() for p in self.parts.values()]).flatten()
        self.body_xyz = body_pose.xyz() #(parts_xyz[0::3].mean(), parts_xyz[1::3].mean(), body_pose.xyz()[2])  # torso z is more informative than mean z
        self.body_rpy = body_pose.rpy()
        self.theta_floor, self.theta_wall, self.dist_to_floor, self.dist_to_wall = self.scene.calc_state_random(self.body_xyz)
        
        r, p, yaw = self.body_rpy
        
        # rot_speed = np.array(
        #     [[np.cos(-yaw), -np.sin(-yaw), 0],
        #      [np.sin(-yaw),  np.cos(-yaw), 0],
        #      [           0,             0, 1]])
        # vx, vy, vz = np.dot(rot_speed, self.robot_body.speed())  # rotate speed back to body point of view
        vx, vy, vz = self.robot_body.speed()

        # floor_dist = self.scene.calc_limb_floor_dist(self.parts, self.foot_list)
        # wall_dist = self.scene.calc_limb_wall_dist(self.parts, self.foot_list)

        vscale = 0.3

        vtar_wall = np.sin(self.theta_wall) * vx + np.cos(self.theta_wall) * vz
        vtar_floor = np.sin(self.theta_floor) * vz + np.cos(self.theta_floor) * vx

        more = np.array([self.theta_floor, self.theta_wall, self.dist_to_floor, self.dist_to_wall, self.body_xyz[1], self.body_xyz[2], # 0-5
            vscale * vx, vscale * vy, vscale * vz, vscale * vtar_floor, vscale * vtar_wall, r, p, yaw], dtype=np.float32)   # 6-13
            # [j] 14-47
            # [ingrasp] 48-51 

        return np.concatenate([more] + [j] + [self.ingrasp])
         
    def update_grasp(self, a):
        for i in range(len(self.foot_list)):
            name = self.foot_list[i]
            info = self.grasp_info[name]

            # you can get contact point info as follows
            # b = info.part.bodies[info.part.bodyIndex]
            # link = info.part.bodyPartIndex
            # contacts = self._p.getContactPoints(bodyA=b, bodyB=0, linkIndexA=link)
            # self._p.getDynamicsInfo(b, link)
 
            info.graspable=True
            graspable_objects = self.graspable_objects

            # disable foot-wall grasp on top wall region
            # if 'foot' in name and self.dist_to_floor < 0.75:
            #     if a[i] < 0 and info.graspid != -1:
            #         info.graspable = False
            #         info.unfix()
            #     graspable_objects = graspable_objects[0::2]

            # decide should we add fix constraint or just determining fix by action value
            hard_constraint = True
            if ('arm' in name and self.dist_to_wall < 2.2 and self.dist_to_wall > 1.0) or \
                ('foot' in name and self.dist_to_floor < 0.75):
                hard_constraint = False

            if info.graspid == -1 and info.can_grasp(graspable_objects):
                for j, foot_idx in enumerate([self.lower_foot_idx, self.upper_foot_idx]):
                    # we deal with foot/arm seperately
                    # 1) if no foot/arm is currently fixed then fix that foot/arm
                    # 2) if the other foot/arm is fixed and current action > 0, then fix this foot/arm and unfix the other one
                    if name in self.foot_list[foot_idx]:
                        other_foot_name = self.foot_list[i + 1] if (i + 1) < foot_idx.stop else self.foot_list[i - 1]
                        other_foot_info = self.grasp_info[other_foot_name]
                        if hard_constraint:
                            if (not self.fixed[j]) or (self.fixed[j] and a[i] > 0):
                                info.fix()
                                self.fixed[j] = True
                                other_foot_info.graspable = False
                                other_foot_info.unfix()
                        else:
                            if a[i] > 0:
                                info.fix()
                                self.fixed[j] = True
                            else:
                                info.graspable = False
                                info.unfix()
                                if other_foot_info.graspid == -1:
                                    self.fixed[j] = False

            info.update_color(self)
            self.ingrasp[i] = 1.0 if info.graspid != -1 else -1.0
         
    def apply_action(self, a):
        Humanoid.apply_action(self, a[0:a.shape[0]-4])
        self.update_grasp(a[a.shape[0]-4:])
          
    def get_color(self, linkIndex):
        c = self.non_graspable_color
        for _,v in self.grasp_info.items():
            if v.part.bodyPartIndex == linkIndex:
                c = v.color
                break
        return c
        
    def get_object_color(self, objectId):
        return self.graspable_object_color if objectId in self.graspable_objects else self.non_graspable_object_color


class GraspInfo():
    
    def __init__(self, robot, part, rel_pos, rel_rad):
        self._p = robot._p
        self.graspable = False
        self.graspid = -1
        self.part = part
        self.rel_pos = rel_pos
        self.rel_rad = rel_rad
        self.color = robot.non_graspable_color
        
    def get_trans(self):
        return np.array(self.part.current_position())
        
    def get_rot(self):
        r = self._p.getMatrixFromQuaternion(self.part.current_orientation())
        return np.array(r).reshape((3,3))
        
    def to_global(self):
        return np.dot(self.get_rot(),self.rel_pos)+self.get_trans()

    def increase_friction(self):
        b = self.part.bodies[self.part.bodyIndex]
        j = self.part.bodyPartIndex
        self._p.changeDynamics(
            bodyUniqueId=b,
            linkIndex=j,
            lateralFriction=2.4)

    def fix(self):
        b = self.part.bodies[self.part.bodyIndex]
        j = self.part.bodyPartIndex
        if self.graspid != -1:
            return
        self.graspid = self._p.createConstraint(
            parentBodyUniqueId=b,
            parentLinkIndex=j,
            childBodyUniqueId=-1,
            childLinkIndex=-1,
            jointType=pybullet.JOINT_POINT2POINT,
            jointAxis=[0,0,0],
            parentFramePosition=self.rel_pos,
            childFramePosition=self.to_global())
         
    def unfix(self):
        if self.graspid == -1:
            return
        else: 
            self._p.removeConstraint(userConstraintUniqueId=self.graspid)
            self.graspid = -1
         
    def update_color(self, robot):
        self.color = robot.in_grasp_color if self.graspid != -1 else (robot.graspable_color if self.graspable else robot.non_graspable_color)
        self._p.changeVisualShape(
            objectUniqueId=self.part.bodies[self.part.bodyIndex], 
            linkIndex=self.part.bodyPartIndex, 
            rgbaColor=self.color)
    
    def can_grasp(self, floors):
        canGrasp=False
        global_pos = self.to_global()
        b = self.part.bodies[self.part.bodyIndex]
        for pt in self.part.contact_list():
            b_other = pt[1] if pt[2] == b else pt[2]
            pos_other = np.array(pt[6] if pt[2] == b else pt[5])
            if b_other in floors:
                if self.rel_rad < 0.0 or np.linalg.norm(global_pos-pos_other) < self.rel_rad:
                    canGrasp=True
                    break
        return canGrasp