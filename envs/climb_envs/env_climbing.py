import numpy as np
import pybullet,scipy.ndimage
from .scene_cliff import *
from .humanoid_climber import *
from pybulletgym.envs.roboschool.env_bases import BaseBulletEnv
from pybulletgym.envs.roboschool.gym_locomotion_envs import WalkerBaseBulletEnv
from collections import deque
import json
import gym
from baselines.ppo1 import mlp_policy
import tensorflow as tf

import os, sys
base_dir = os.path.dirname(os.path.abspath(__file__)) + '/../../'
sys.path.insert(0, os.path.join(base_dir, 'tools'))
result_path = base_dir + 'results/'

#this is base-class for environment
class ClimberBaseBulletEnv(WalkerBaseBulletEnv):
    foot_ground_object_names = set([])
    num_run = 0
    warm_up = 3000
    frame = 0
    base_alive = 1

    def __init__(self, robot, graspable_names, render=False, *, nr_level=2, theta_change_interval=1,
        random=False):
        WalkerBaseBulletEnv.__init__(self, robot, render)
        self.graspable_names = graspable_names
        self.show_debug_info = False
        self.fix_camera = True
        self.nr_level = nr_level
        self.theta_change_interval = theta_change_interval
        self.random = random

        self.state_list = []
        self.init_ref = False
        self.count = 0
        self.next_env = None
        self.next_pi = None
        self.exp_name = ''
    
    def create_single_player_scene(self, bullet_client):
        self.cliff_scene = CliffScene(bullet_client, gravity=9.8, timestep=0.0165/4, frame_skip=4, 
            theta_floor=0, theta_wall=0, nr_level=self.nr_level)
        self.cliff_scene.episode_restart(self._p)
        self.foot_ground_object_names = set(self.cliff_scene.floor_and_wall_names)
        return self.cliff_scene

    def _reset(self):
        if self.base_alive > 0:
            self.base_alive *= 0.9999
        # if self.frame > 1000:
        #     if self.warm_up > 0:
        #         self.warm_up -= 1
        #     else:
        #         self.num_run = (self.num_run + 1) % 2000

        if self.stateId >= 0:
            #print("restoreState self.stateId:",self.stateId)
            self._p.restoreState(self.stateId)

        if self.init_ref:
            self.init_state_info = self.init_path_id + '_' + str(np.random.randint(self.init_max_iter))
            self.robot.init_state_info = self.init_state_info

        self.count += 1
        if self.random and hasattr(self, 'cliff_scene') and self.count % self.theta_change_interval == 0:
            self.cliff_scene.theta_floor, self.cliff_scene.theta_wall = self.get_random_theta()

        r = BaseBulletEnv._reset(self)
        r = self.state_mapping(r)
        self._p.configureDebugVisualizer(pybullet.COV_ENABLE_RENDERING,0)
        self.parts, self.jdict, self.ordered_joints, self.robot_body = self.robot.addToScene(self._p, self.cliff_scene.ground_plane_mjcf)
        self.ground_ids = set([(self.parts[f].bodies[self.parts[f].bodyIndex], self.parts[f].bodyPartIndex) for f in self.foot_ground_object_names])
        self.ground_ids_ = set([(self.parts[f].bodies[self.parts[f].bodyIndex], self.parts[f].bodyPartIndex) for f in self.foot_ground_object_names if f.startswith('floor')])
        
        if self.random:
            self._p.removeState(self.stateId)
            self.stateId=self._p.saveState()
        elif self.stateId < 0:
            self.stateId=self._p.saveState()
            #print("saving state self.stateId:",self.stateId)

        if not self.init_ref:
            self._reset_pos()

        self._p.configureDebugVisualizer(pybullet.COV_ENABLE_RENDERING,1)

        # find graspable object
        self.graspable_objects = []
        for k,v in self.parts.items():
            is_graspable_object = False
            for name in self.graspable_names:
                if name in k:
                    is_graspable_object = True
            if is_graspable_object:
                self.graspable_objects.append(v.bodies[v.bodyIndex])
            #self._p.changeVisualShape(objectUniqueId=v.bodies[v.bodyIndex], linkIndex=-1, 
            #                          rgbaColor=self.robot.graspable_object_color if is_graspable_object else self.robot.non_graspable_object_color)
        self.robot.graspable_objects = self.graspable_objects

        #change color
        for j in range(self._p.getNumJoints(self.robot_body.bodies[self.robot_body.bodyIndex])):
            self._p.changeVisualShape(objectUniqueId=self.robot_body.bodies[self.robot_body.bodyIndex], 
                                      linkIndex=j, rgbaColor=self.robot.non_graspable_color)
        self._p.changeVisualShape(objectUniqueId=self.robot_body.bodies[self.robot_body.bodyIndex], 
                                  linkIndex=-1, rgbaColor=self.robot.non_graspable_color)
        
        #return
        return r
        
    #this is one place you want to tune, this defines the reward function
    def _step(self, a):
        self.a = a
        self.frame = self.frame+1
        if not self.scene.multiplayer:  # if multiplayer, action first applied to all robots, then global step() called, then _step() for all robots with the same actions
            self.robot.apply_action(a)
            self.scene.global_step()

        state = self.robot.calc_state()  # also calculates self.joints_at_limit
        observation = self.state_mapping(state)

        done = False
        if not np.isfinite(state).all():
            print("~INF~", state)
            done = True

        # potential_old = self.potential
        # self.potential = self.robot.calc_potential()
        # progress = float(self.potential - potential_old)

        for i,f in enumerate(self.robot.feet): # TODO: Maybe calculating feet contacts could be done within the robot code
            contact_ids = set((x[2], x[4]) for x in f.contact_list())
            #print("CONTACT OF '%d' WITH %d" % (contact_ids, ",".join(contact_names)) )
            if self.ground_ids & contact_ids:
                #see Issue 63: https://github.com/openai/roboschool/issues/63
                #feet_collision_cost += self.foot_collision_cost
                self.robot.feet_contact[i] = 1.0
            else:
                self.robot.feet_contact[i] = 0.0

        electricity_cost  = self.electricity_cost  * float(np.abs(a[0:a.shape[0]-4]*self.robot.joint_speeds).mean())  # let's assume we have DC motor with controller, and reverse current braking
        electricity_cost += self.stall_torque_cost * float(np.square(a[0:a.shape[0]-4]).mean())
        electricity_cost /= 5

        joints_at_limit_cost = float(self.joints_at_limit_cost * self.robot.joints_at_limit)

        other_rewards, other_rew_dict, other_done = self._get_reward(observation)

        next_rew = 0
        if self.next_env is not None and other_done:
            if not hasattr(self, 'next_rew_weight'):
                self.next_rew_weight = 1
            next_rew = self.next_rew_weight * self.eval_next_env()

        self.rewards = [
            #progress,
            electricity_cost,
            joints_at_limit_cost,
            next_rew,
            ] + other_rewards

        rew_dict = {'electricity': electricity_cost, 'joints': joints_at_limit_cost, 'next': next_rew}
        rew_dict.update(other_rew_dict)

        done = done or other_done

        self.HUD(state, a, done)
        
        return observation, sum(self.rewards), bool(done), rew_dict
        
    def _render(self, mode, close=False):
        if self.isRender and self.fix_camera:
            self._p.resetDebugVisualizerCamera(
                cameraDistance=self._cam_dist,
                cameraYaw=self._cam_yaw,
                cameraPitch=self._cam_pitch,
                cameraTargetPosition=self.robot.body_xyz)
        img = WalkerBaseBulletEnv._render(self, mode, close)
        #render dist_to_floor, dist_to_wall, dist_to_target
        xyz = self.robot.body_xyz
        if self.isRender and hasattr(self, '_p'):
            self._p.removeAllUserDebugItems()  
            if self.show_debug_info:  
                self._p.addUserDebugLine(lineFromXYZ=(xyz[0],xyz[1],xyz[2]),
                                         lineToXYZ=(xyz[0],xyz[1],xyz[2]-self.robot.dist_to_floor),
                                         lineColorRGB=(1,0,0),lineWidth=2,lifeTime=0)
                self._p.addUserDebugLine(lineFromXYZ=(xyz[0],xyz[1],xyz[2]),
                                         lineToXYZ=(xyz[0]+self.robot.dist_to_wall,xyz[1],xyz[2]),
                                         lineColorRGB=(0,1,0),lineWidth=2,lifeTime=0)
        #povray 
        if hasattr(self,'povrayRenderer'):
            path = self.povrayRenderer.render(self)
            img = scipy.ndimage.imread(path)
        return img

    def _reset_pos(self):
        pass

    def set_curriculum(self, curriculum):
        pass

    def set_params(self, seed=0, exp_name=None, next_env_iter=None, init_state_id=None, init_state_iter=None, curriculum=0):
        self.seed(seed)
        if exp_name:
            self.exp_name = exp_name
        if next_env_iter:
            self.set_next_env()
        if init_state_id and init_state_iter > 0:
            self.load_init_state(init_state_id, init_state_iter)
        self.set_curriculum(curriculum)

    def get_state(self):
        return self.robot.calc_state()

    def state_mapping(self, state):
        return state

    def reset_other_env(self, env):
        env.frame = 0
        env.scene = self.scene
        return env.state_mapping(self.get_state())

    def step_with_other_env(self, ob, env, pi):
        env.frame += 1

        ac, _ = pi.act(False, ob)
        self.robot.apply_action(ac)
        self.scene.global_step()
        state = self.get_state()
        ob = env.state_mapping(state)

        rews, info, done = env._get_reward(ob)

        electricity_cost  = self.electricity_cost  * float(np.abs(ac[0:ac.shape[0]-4]*self.robot.joint_speeds).mean())  # let's assume we have DC motor with controller, and reverse current braking
        electricity_cost += self.stall_torque_cost * float(np.square(ac[0:ac.shape[0]-4]).mean())
        electricity_cost /= 5
        joints_at_limit_cost = float(self.joints_at_limit_cost * self.robot.joints_at_limit)
        rews += [electricity_cost, joints_at_limit_cost]

        rew = sum(rews)
        if not np.isfinite(state).all():
            done = True

        return ob, rew, done, info

    def set_next_env(self, ob_prefix='next_'):
        if not hasattr(self, 'next_env_id'):
            return
        self.next_env = gym.make(self.next_env_id).env
        with tf.variable_scope('next_' + self.next_env_id):
            self.next_pi = mlp_policy.MlpPolicy(name="pi", ob_space=self.next_env.observation_space, ac_space=self.next_env.action_space,
                                        hid_layer_sizes=(128, 64), ob_prefix=ob_prefix)
        return self.next_pi

    def eval_next_env(self):
        if self.next_env is None or self.next_pi is None:
            return 0
        
        ob = self.reset_other_env(self.next_env)
        done = False
        cum_rew = 0

        while not done:
            ob, rew, done, _ = self.step_with_other_env(ob, self.next_env, self.next_pi)
            cum_rew += rew
        return cum_rew

    def load_init_state(self, identifier, iter):
        self.robot.reset_ref = True
        self.init_ref = True
        self.init_path_id, self.init_max_iter = result_path + 'saved_state/' + identifier, iter

    def save_final_state(self, identifier, iter):
        os.makedirs(result_path + 'saved_state/', exist_ok=True)
        filename = result_path + 'saved_state/' + identifier + '_' + str(iter)
        self._p.saveBullet(bulletFileName=filename)
        if self.random:
            with open(filename + '.json', 'w') as f:
                json.dump([self.cliff_scene.theta_floor, self.cliff_scene.theta_wall], f)
        print('State saved')


class HumanoidWalkerClimberBulletEnv(ClimberBaseBulletEnv):
    def __init__(self, robot=RandomHumanoidClimber()):
        self.robot = robot
        ClimberBaseBulletEnv.__init__(self, self.robot, ['wall'])
        self.electricity_cost  = 4.25*WalkerBaseBulletEnv.electricity_cost
        self.stall_torque_cost = 4.25*WalkerBaseBulletEnv.stall_torque_cost


class RandomEnv(ClimberBaseBulletEnv):

    def __init__(self, robot=RandomHumanoidClimber(), *, nr_level=2, theta_change_interval=1, random=True):
        self.robot = robot
        ClimberBaseBulletEnv.__init__(self, self.robot, ['wall', 'floor'], nr_level=nr_level, theta_change_interval=theta_change_interval,
            random=random)
        self.electricity_cost  = 4.25*WalkerBaseBulletEnv.electricity_cost
        self.stall_torque_cost = 4.25*WalkerBaseBulletEnv.stall_torque_cost

    def get_random_theta(self):
        if self.init_ref:
            if os.path.exists(self.init_state_info + '.json'):
                with open(self.init_state_info + '.json', 'r') as f:
                    return json.load(f)
        return np.random.uniform(*self.theta_floor_range) * math.pi / 180, np.random.uniform(*self.theta_wall_range) * math.pi / 180

    def create_single_player_scene(self, bullet_client):
        theta_floor, theta_wall = self.get_random_theta()
        self.cliff_scene = RandomCliffScene(bullet_client, gravity=9.8, timestep=0.0165/4, frame_skip=4, 
                                      sz_floor=3, width_floor=3, sz_wall=3, thick=0.1,
                                      theta_floor=theta_floor, theta_wall=theta_wall, nr_level=self.nr_level, exp_name=self.exp_name)
        self.cliff_scene.episode_restart(self._p)
        self.foot_ground_object_names = set(self.cliff_scene.floor_and_wall_names)
        return self.cliff_scene


class RandomWalkerEnv(RandomEnv):

    theta_floor_range = [0, 30]
    theta_wall_range = [0, 30]

    # reward
    velocity_reward = 5
    straight_cost = -5
    alive = 0.5

    # target
    target_speed = 0.5

    # next
    next_env_id = 'RandomClimberEnv-v0'
    next_rew_weight = 0.5

    def _get_observation_space(self, robot):
        '''
        theta_floor, theta_wall, dist_to_floor, body_xyz[1],
        vx, vy, vz, vtar, r, p, yaw,
        [j]
        '''
        OBS_DIM = 45
        high = np.resize(robot.observation_space.high, OBS_DIM)
        low = np.resize(robot.action_space.low, OBS_DIM)
        return gym.spaces.Box(high, low, dtype=np.float32)

    def state_mapping(self, state):
        index = np.array(list(range(0, 3)) + [4] + list(range(6, 10)) + list(range(11, 48)))
        return np.clip(state[index], -3, 3)

    def _get_reward(self, ob):
        info = {}
        info['velocity'] = self.velocity_reward * np.clip(np.cos(ob[10]) * ob[7], -self.target_speed, self.target_speed)
        info['straight'] = self.straight_cost * np.abs(ob[3])
        info['alive'] = self.alive * (2 if ob[2] > 0.78 / np.cos(ob[0]) else -1)

        floor_wall_x_range = (np.cos(self.scene.theta_floor) + np.sin(self.scene.theta_wall)) * self.scene.sz_floor
        curr_x = self.robot.body_xyz[0] % floor_wall_x_range
        max_x = np.cos(ob[0]) * self.scene.sz_floor - 0.4
        success_done = curr_x > max_x and curr_x < max_x + 0.4

        info['success'] = 100 if success_done else 0
        reward = list(info.values())

        info['final'] = True if success_done else False
        done = info['alive'] < 0 or np.cos(ob[10]) < 0.5 or success_done

        return reward, info, done
    
    def _reset_pos(self):
        sz_floor, theta = self.scene.sz_floor, self.scene.theta_floor
        init_pos = [np.random.uniform(0.2, 1.2), 0, 1.2]
        x = sz_floor - (sz_floor - init_pos[0]) * np.cos(theta)
        y = init_pos[1]
        z = np.tan(theta) * x + 0.1
        self.robot_body.reset_position([x, y, z])

        done = False
        count = 0
        while not done:
            max_dist = np.max(self.scene.calc_limb_floor_dist(self.robot.parts, self.robot.foot_list))
            if max_dist > 5 and count < 20:
                z += 0.1
                count += 1
                self.robot_body.reset_position([x, y, z])
            else:
                done = True


class RandomClimberEnv(RandomEnv):

    theta_floor_range = [0, 30]
    theta_wall_range = [0, 30]

    # reward
    velocity_reward = 10
    straight_cost = -2
    wall_dist_reward = 0
    alive = 5

    # target
    target_speed = 0.5

    # next
    next_env_id = 'RandomStanderEnv-v0'
    next_rew_weight = 2

    def set_curriculum(self, curriculum):
        if curriculum == 0:
            self.velocity_reward = 10
            self.alive = 5
        elif curriculum == 1:
            self.velocity_reward = 20
            self.alive = 0.2

    def _get_observation_space(self, robot):
        '''
        theta_floor, theta_wall, alive, dist_to_wall, body_xyz[1],
        vx, vy, vz, vtar, r, p, yaw,
        [j]
        [ingrasp]
        '''
        OBS_DIM = 50
        high = np.resize(robot.observation_space.high, OBS_DIM)
        low = np.resize(robot.action_space.low, OBS_DIM)
        return gym.spaces.Box(high, low, dtype=np.float32)

    def state_mapping(self, state):
        state[2] = np.float32(bool(state[2] > 0.78 / np.cos(state[0])))
        index = np.array(list(range(0, 5)) + list(range(6, 9)) + list(range(10, 52)))
        return np.clip(state[index], -3, 3)

    def _get_reward(self, ob):

        info = {}
        info['velocity'] = self.velocity_reward * np.clip(np.cos(ob[10]) * ob[8], -self.target_speed, self.target_speed)
        info['straight'] = self.straight_cost * np.abs(ob[4])
        info['wall_dist'] = self.wall_dist_reward / (1 + np.abs(ob[3] - 0.3))
        info['alive'] = self.alive * (2 if ob[2] > 0.5 else -1)

        floor_wall_z_range = (np.cos(self.scene.theta_wall) + np.sin(self.scene.theta_floor)) * self.scene.sz_floor
        curr_z = (self.robot.body_xyz[2] + np.sin(self.scene.theta_floor)) % floor_wall_z_range - np.sin(self.scene.theta_floor)
        max_z = np.cos(self.scene.theta_wall) * self.scene.sz_floor
        success_done = curr_z > max_z

        info['success'] = 100 if success_done else 0
        reward = list(info.values())

        info['final'] = True if success_done else False
        done = self.frame > 200 or info['alive'] < 0 or np.cos(ob[10]) < 0.5 or np.abs(ob[9]) > 0.6 or success_done

        return reward, info, done
    
    def _reset_pos(self):
        sz_floor, theta = self.scene.sz_floor, self.scene.theta_floor
        init_pos = [np.cos(theta) * sz_floor - 0.4, 0, 1.2]
        x, y = init_pos[0], init_pos[1]
        z = np.tan(theta) * x + 0.1
        self.robot_body.reset_position([x, y, z])

        done = False
        count = 0
        while not done:
            max_dist = np.max(self.scene.calc_limb_floor_dist(self.robot.parts, self.robot.foot_list))
            if max_dist > 5 and count < 20:
                z += 0.1
                count += 1
                self.robot_body.reset_position([x, y, z])
            else:
                done = True


class RandomStanderEnv(RandomEnv):

    theta_floor_range = [0, 30]
    theta_wall_range = [0, 30]

    velocity_reward = 20
    straight_cost = -2
    height_reward = 4
    alive = 0.5

    # target
    target_speed = 0.5

    # next
    next_env_id = 'RandomWalkerEnv-v0'
    next_rew_weight = 4

    def _get_observation_space(self, robot):
        '''
        theta_floor, theta_wall, dist_to_floor, body_xyz[1],
        vx, vy, vz, vtar, r, p, yaw,
        [j]
        [ingrasp]
        '''
        OBS_DIM = 49
        high = np.resize(robot.observation_space.high, OBS_DIM)
        low = np.resize(robot.action_space.low, OBS_DIM)
        return gym.spaces.Box(high, low, dtype=np.float32)

    def state_mapping(self, state):
        state[2] = (state[2] - self.scene.sz_floor) if state[2] > self.scene.sz_floor / 2 else state[2]
        index = np.array(list(range(0, 3)) + [4] + list(range(6, 10)) + list(range(11, 52)))
        return np.clip(state[index], -3, 3)

    def _get_reward(self, ob):
        info = {}
        info['velocity'] = self.velocity_reward * np.clip(ob[7], -self.target_speed, self.target_speed)
        info['straight'] = self.straight_cost * (np.abs(ob[3]) + np.abs(ob[8]))
        info['height'] = self.height_reward * np.clip(ob[2], 0, 1.0)
        info['alive'] = self.alive

        floor_wall_x_range = (np.cos(self.scene.theta_floor) + np.sin(self.scene.theta_wall)) * self.scene.sz_floor
        curr_x = self.robot.body_xyz[0] % floor_wall_x_range
        max_x = 1.0
        min_x = np.cos(self.scene.theta_floor) * self.scene.sz_floor
        success_done = curr_x > max_x and curr_x < min_x and ob[2] > 0.8

        info['success'] = 100 if success_done else 0
        reward = list(info.values())

        info['final'] = True if success_done else False
        done = self.frame > 150 or ob[7] < -0.5 or np.abs(ob[8]) > 0.5 or success_done

        return reward, info, done


class End2EndEnv(HumanoidWalkerClimberBulletEnv):

    extra_alive = 4 # 20

    def _get_reward(self, state):

        alive = float(self.robot.alive_bonus(state[2]+self.robot.initial_z, self.robot.body_rpy[1]))   # state[2] is body height above ground, body_rpy[1] is pitch
        extra_alive = self.extra_alive

        if state[3] < 0.4:
            extra_alive += state[2] + state[7]
        else:
            extra_alive += state[5]

        done = alive < 0 or state[2] < 0 or state[7] < -1

        if done:
            extra_alive = -100

        done = done or self.frame > 400 or state[2] > 4

        return [alive + extra_alive], {'alive': alive + extra_alive}, done

    def _reset_pos(self):
        x = np.random.uniform(0.35, 5 - 0.35)
        self.robot_body.reset_position([x, 0, 1.2])
        # self.robot_body.reset_position([5 - 0.35, 0, 1.2])
