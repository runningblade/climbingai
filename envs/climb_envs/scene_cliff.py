import os, inspect, math, random, shutil
import xml.etree.cElementTree as ET
import numpy as np
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0,parentdir)

from baselines.common import mpi_config
useMPI=mpi_config.try_import_mpi()
if useMPI:
    from mpi4py import MPI

from pybulletgym.envs.roboschool.scene_bases import *
from pybulletgym.envs.roboschool.scene_stadium import *
import pybullet

class Plane():
    def __init__(self, curr_pos0, curr_pos):
        self.eps=1.0e-6
        self.v0=np.array(curr_pos0)
        self.curr_pos0=curr_pos0
        self.curr_pos=curr_pos
        self.v=np.array(curr_pos)
        #n
        self.n=np.array([curr_pos0[2]-curr_pos[2],0,curr_pos[0]-curr_pos0[0]])
        self.n/=np.linalg.norm(self.n)
        #x
        self.min_x=min(curr_pos0[0],curr_pos[0])
        self.max_x=max(curr_pos0[0],curr_pos[0])
        #z
        self.min_z=min(curr_pos0[2],curr_pos[2])
        self.max_z=max(curr_pos0[2],curr_pos[2])
    def dist_to_floor(self, pos_xyz):
        if abs(self.n[2])<1.0e-6:
            return -1
        if pos_xyz[0]>=self.min_x-self.eps and pos_xyz[0]<=self.max_x+self.eps:
            dist=np.dot(np.array(pos_xyz)-self.v0,self.n)/self.n[2]
            if dist<0:
                return -1
            else: return dist
        else: return -1
    def dist_to_wall(self, pos_xyz):
        if abs(self.n[0])<1.0e-6:
            return -1
        if pos_xyz[2]>=self.min_z-self.eps and pos_xyz[2]<=self.max_z+self.eps:
            dist=self.dist_to_wall_inner(pos_xyz)
            if dist<0.0:
                return -1
            else: return dist
        else: return -1
    def dist_to_floor_random(self, pos_xyz):
        if abs(self.n[2])<1.0e-6:
            return None
        if pos_xyz[2]>=self.min_z-self.eps and pos_xyz[0]>=self.min_x-self.eps:
            dist=np.dot(np.array(pos_xyz)-self.v0,self.n)/self.n[2]
            return dist
        else: return None
    def dist_to_wall_random(self, pos_xyz):
        if abs(self.n[0])<1.0e-6:
            return None
        if pos_xyz[0]<=self.max_x+self.eps:
            dist=self.dist_to_wall_inner(pos_xyz)
            return dist
        else: return None
    def dist_to_wall_inner(self, pos_xyz):
        return -np.dot(np.array(pos_xyz)-self.v0,self.n)/self.n[0]
    def block_wall(self, pos1_xyz, pos2_xyz):
        if self.dist_to_wall_inner(pos2_xyz)>=0.0:
            #pos2 is not blocked by wall, then return immediately
            return True,False,pos1_xyz
        #check if 2D line segments intersect
        L0p=(pos1_xyz[0],pos1_xyz[2])
        L0d=(pos2_xyz[0]-pos1_xyz[0],pos2_xyz[2]-pos1_xyz[2])
        L1p=(self.curr_pos0[0],self.curr_pos0[2])
        L1d=(self.curr_pos[0]-self.curr_pos0[0],self.curr_pos[2]-self.curr_pos0[2])
        #we want to solve L0p+alpha*L0d=L1p+beta*L1d
        RHS=(self.curr_pos0[0]-pos1_xyz[0],self.curr_pos0[2]-pos1_xyz[2])
        a,b,c,d=L0d[0],L1d[0],L0d[1],L1d[1]
        #the inverse of 2x2 matrix (a b) is: ( d -b)
        #                          (c d)     (-c  a)/(ad-bc)
        alpha= ( d*RHS[0]-b*RHS[1])/(a*d-b*c)
        beta =-(-c*RHS[0]+a*RHS[1])/(a*d-b*c)
        #pt0=(L0p[0]+alpha*L0d[0],L0p[1]+alpha*L0d[1])
        #pt1=(L1p[0]+beta *L1d[0],L1p[1]+beta *L1d[1])
        if alpha<0.0 or alpha>1.0 or beta<0.0 or beta>1.0:
            return False,False,pos1_xyz
        else:
            dpos=(self.curr_pos[0]-L0p[0],self.curr_pos[2]-L0p[1])
            return False,True,self.curr_pos
    def dist_to_turning(self, pos_xyz):
        dist_vec=np.array(pos_xyz)-self.v
        dist=np.sqrt(dist_vec[0]**2+dist_vec[2]**2)
        if dist_vec[0]>0 or dist_vec[2]<0:
            return -1
        else: return dist

class CliffScene(Scene):
    multiplayer = False
    cliffLoaded = 0
    
    #these parameters can be changed to change the environment geometry
    def __init__(self, bullet_client, gravity, timestep, frame_skip, *,
                 theta_floor=0, theta_wall=0, sz_floor=5, width_floor=5, sz_wall=5, thick=0.5, nr_level=15, exp_name=''):
        Scene.__init__(self, bullet_client, gravity, timestep, frame_skip)
        self.theta_floor=theta_floor
        self.theta_wall=theta_wall
        self.sz_floor=sz_floor
        self.width_floor=width_floor
        self.sz_wall=sz_wall
        self.thick=thick
        self.nr_level=nr_level
        self.infty=max(self.sz_floor,self.sz_wall)*nr_level
        self.exp_name = exp_name

    def indent(self, elem, level=0):
        i = "\n" + level*"  "
        j = "\n" + (level-1)*"  "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for subelem in elem:
                self.indent(subelem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = j
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = j
        return elem 

    def create_inertial(self, root):
        inertial = ET.SubElement(root,"inertial")
        ET.SubElement(inertial,"mass").text="0"
        inertia = ET.SubElement(inertial,"inertia")
        ET.SubElement(inertia,"ixx").text="0.166667"
        ET.SubElement(inertia,"ixy").text="0"
        ET.SubElement(inertia,"ixz").text="0"
        ET.SubElement(inertia,"iyy").text="0.166667"
        ET.SubElement(inertia,"iyz").text="0"
        ET.SubElement(inertia,"izz").text="0.166667"

    def create_visual(self, root, path):
        visual=ET.SubElement(root,"visual",name="visual")
        ET.SubElement(visual,"cast_shadows").text="0"
        geometry = ET.SubElement(visual,"geometry")
        mesh = ET.SubElement(geometry,"mesh")
        ET.SubElement(mesh,"uri").text=path+".obj"
        
        #material = ET.SubElement(visual,"material")
        #ET.SubElement(mesh,"ambient").text="0.0 0.0 0.0 1.0"
        #ET.SubElement(mesh,"diffuse").text="0.0 0.0 0.0 1.0"
        #ET.SubElement(mesh,"specular").text="0.0 0.0 0.0 1.0"
        #ET.SubElement(mesh,"emissive").text="0.0 0.0 0.0 1.0"

    def create_mesh(self, root, path):
        model = ET.SubElement(root,"model",name=path)
        ET.SubElement(model,"static").text="1"
        link = ET.SubElement(model,"link",name=path)
        self.create_inertial(link)
        self.create_visual(link, path)
        collision = ET.SubElement(link,"collision")
        geometry = ET.SubElement(collision,"geometry")
        mesh = ET.SubElement(geometry,"mesh")
        ET.SubElement(mesh,"uri").text=path+".obj"

    def create_cliff(self, path):
        root = ET.Element("sdf",version="1.6")
        world = ET.SubElement(root,"world",name="default")
        ET.SubElement(world,"gravity").text="0 0 -9.8"

        for i in range(self.nr_level):
            self.create_mesh(world,"floor"+str(i))
            self.create_mesh(world,"wall"+str(i))

        self.indent(root)
        tree = ET.ElementTree(root)
        tree.write(path)

    def write_v(self, f, x, y, z):
        f.write('v %f %f %f\n'%(x,y,z))
        
    def write_f(self, f, x, y, z, w):
        f.write('f %d %d %d %d\n'%(x,y,z,w))

    def init_pos(self):
        return (0,0,-self.sz_floor*math.sin(self.theta_floor))

    def create_floor(self, path, from_ctr, mtl):
        f=open(path,"w")
        f.write("mtllib cliff.mtl\n")
        f.write("usemtl "+mtl+"\n\n")
        #vertices
        dxThick=self.thick*math.sin(self.theta_wall)
        dzThick=self.thick*math.cos(self.theta_wall)
        dx=self.sz_floor*math.cos(self.theta_floor)
        dz=self.sz_floor*math.sin(self.theta_floor)
        to_ctr=(from_ctr[0]+dx,from_ctr[1],from_ctr[2]+dz)
        #-x
        self.write_v(f,from_ctr[0]        ,-self.width_floor,from_ctr[2]        )
        self.write_v(f,from_ctr[0]-dxThick,-self.width_floor,from_ctr[2]-dzThick)
        self.write_v(f,from_ctr[0]-dxThick, self.width_floor,from_ctr[2]-dzThick)
        self.write_v(f,from_ctr[0]        , self.width_floor,from_ctr[2]        )
        #+x
        self.write_v(f,  to_ctr[0]        ,-self.width_floor,  to_ctr[2]        )
        self.write_v(f,  to_ctr[0]-dxThick,-self.width_floor,  to_ctr[2]-dzThick)
        self.write_v(f,  to_ctr[0]-dxThick, self.width_floor,  to_ctr[2]-dzThick)
        self.write_v(f,  to_ctr[0]        , self.width_floor,  to_ctr[2]        )
        #indices
        f.write("s off\n")
        self.write_f(f,4,3,2,1)
        self.write_f(f,2,6,5,1)
        self.write_f(f,3,7,6,2)
        self.write_f(f,8,7,3,4)
        self.write_f(f,5,8,4,1)
        self.write_f(f,6,7,8,5)
        f.close()
        return to_ctr
    
    def create_wall(self, path, from_ctr, mtl):
        f=open(path,"w")
        f.write("mtllib cliff.mtl\n")
        f.write("usemtl "+mtl+"\n\n")
        #vertices
        dzThickF=self.thick*math.sin(self.theta_floor)
        dxThickF=self.thick*math.cos(self.theta_floor)
        dzThick=self.thick*math.cos(self.theta_wall)
        dxThick=self.thick*math.sin(self.theta_wall)
        dx=self.sz_wall*math.sin(self.theta_wall)
        dz=self.sz_wall*math.cos(self.theta_wall)
        to_ctr=(from_ctr[0]+dx,from_ctr[1],from_ctr[2]+dz)
        #-x
        self.write_v(f,  to_ctr[0],-self.width_floor,  to_ctr[2])
        self.write_v(f,from_ctr[0]-dxThick ,-self.width_floor,from_ctr[2]-dzThick)
        self.write_v(f,from_ctr[0]-dxThick , self.width_floor,from_ctr[2]-dzThick)
        self.write_v(f,  to_ctr[0], self.width_floor,  to_ctr[2])
        #+x
        self.write_v(f,  to_ctr[0]+dxThickF         ,-self.width_floor,  to_ctr[2]+dzThickF)
        self.write_v(f,from_ctr[0]-dxThick +dxThickF,-self.width_floor,from_ctr[2]-dzThick+dzThickF)
        self.write_v(f,from_ctr[0]-dxThick +dxThickF, self.width_floor,from_ctr[2]-dzThick+dzThickF)
        self.write_v(f,  to_ctr[0]+dxThickF         , self.width_floor,  to_ctr[2]+dzThickF)
        #indices
        f.write("s off\n")
        self.write_f(f,4,3,2,1)
        self.write_f(f,2,6,5,1)
        self.write_f(f,3,7,6,2)
        self.write_f(f,8,7,3,4)
        self.write_f(f,5,8,4,1)
        self.write_f(f,6,7,8,5)
        f.close()
        return to_ctr

    def episode_restart(self, bullet_client):
        self._p = bullet_client
        Scene.episode_restart(self, bullet_client)
        if self.cliffLoaded == 0:
            self.cliffLoaded = 1

            self.floors = []
            self.walls = []
            self.floor_and_wall_names=[]
            dirname = os.path.join(os.path.dirname(__file__), "..", "pybulletgym", "envs", "assets", "scenes", "cliff")
            curr_pos = self.init_pos()
            pts=[]
            for i in range(self.nr_level):
                self.floor_and_wall_names.append("floor"+str(i))
                self.floor_and_wall_names.append("wall"+str(i))
                #add floor
                curr_pos0 = curr_pos
                curr_pos = self.create_floor(os.path.join(dirname, "floor"+str(i)+".obj"),curr_pos0,"floor")
                self.floors.append(Plane(curr_pos0, curr_pos))
                pts+=[self.floors[-1].curr_pos0,self.floors[-1].curr_pos]
                #add wall
                curr_pos0 = curr_pos
                curr_pos = self.create_wall(os.path.join(dirname, "wall" +str(i)+".obj"),curr_pos0,"wall" )
                self.walls.append(Plane(curr_pos0, curr_pos))
                pts+=[self.walls[-1].curr_pos0,self.walls[-1].curr_pos]
            if not useMPI or MPI.COMM_WORLD.Get_rank() == 0:
                self.create_cliff(os.path.join(dirname, "cliff.sdf"))
                self.write_pts_vtk(pts, 'cliff.vtk')
            if hasattr(self, 'ground_plane_mjcf'):
                for i in self.ground_plane_mjcf:
                    self._p.removeBody(i)
            if useMPI:
                MPI.COMM_WORLD.Barrier()
            self.ground_plane_mjcf=self._p.loadSDF(os.path.join(dirname, "cliff.sdf"))

            # this is to debug the floor levels
            # for f in self.floors:
            #    print(f.min_x,f.max_x)

            for i in self.ground_plane_mjcf:
                self._p.changeDynamics(i, -1, lateralFriction=0.8, restitution=0.5)
                self._p.changeVisualShape(i,-1,rgbaColor=[1,1,1,0.8])
                
            self._p.configureDebugVisualizer(pybullet.COV_ENABLE_PLANAR_REFLECTION,1)
                
    #let's do some debug
    #print(self.calc_state((9.9,0,17)))

    def get_stage(self, pos_xyz):
        #test floors first
        for i in range(0,len(self.floors)):
            f=self.floors[len(self.floors)-1-i]
            dist=f.dist_to_floor(pos_xyz)
            if dist>=0:
                return len(self.floors)-1-i
        return -1

    def floor_dist_random(self, pos_xyz):
        for f in self.floors[::-1]:
            dist = f.dist_to_floor_random(pos_xyz)
            if dist is not None:
                return dist
        return 0

    def wall_dist_random(self, pos_xyz):
        for f in self.walls:
            dist = f.dist_to_wall_random(pos_xyz)
            if dist is not None:
                return dist
        return 0
    
    def floor_dist(self, pos_xyz):
        #test floors first
        for i in range(0,len(self.floors)):
            f=self.floors[len(self.floors)-1-i]
            dist=f.dist_to_floor(pos_xyz)
            if dist>=0:
                return dist
        #test walls in case of sloped wall
        for i in range(0,len(self.walls)):
            f=self.walls[len(self.walls)-1-i]
            dist=f.dist_to_floor(pos_xyz)
            if dist>=0:
                return dist
        return self.infty
    
    def wall_dist(self, pos_xyz):
        #test walls first
        pos_xyz[2] = np.maximum(0, pos_xyz[2])
        for i in range(0,len(self.walls)):
            w=self.walls[i]
            dist=w.dist_to_wall(pos_xyz)
            if dist>=0:
                return dist
        #test floors in case of sloped floor
        for i in range(0,len(self.floors)):
            w=self.floors[i]
            dist=w.dist_to_wall(pos_xyz)
            if dist>=0:
                return dist
        return self.infty

    def turning_dist(self, pos_xyz):
        for i in range(0,len(self.walls)):
            f=self.walls[i]
            dist=f.dist_to_turning(pos_xyz)
            if dist>=0:
                return dist
        return self.infty
    
    def calc_state(self, body_xyz):
        return self.theta_floor, self.theta_wall, self.floor_dist(body_xyz), self.wall_dist(body_xyz)

    def calc_state_random(self, body_xyz):
        return self.theta_floor, self.theta_wall, self.floor_dist_random(body_xyz), self.wall_dist_random(body_xyz)

    def calc_limb_wall_dist(self, part, limbs):
        return np.array([self.wall_dist(part[limb].pose().xyz()) for limb in limbs])

    def calc_limb_floor_dist(self, part, limbs):
        return np.array([self.floor_dist(part[limb].pose().xyz()) for limb in limbs])

    def calc_limb_turning_dist(self, part, limbs):
        return np.array([self.turning_dist(part[limb].pose().xyz()) for limb in limbs])

    def calc_limb_body_z_dist(self, part, limbs, body_z):
        return np.array([part[limb].pose().xyz()[2] for limb in limbs]) - body_z

    def calc_geodesic_distance(self, pos1_xyz, pos2_xyz):
        if pos1_xyz[0]>pos2_xyz[0]:
            pos1_xyz,pos2_xyz=pos2_xyz,pos1_xyz
        #compute in-between vertices
        poses=[pos1_xyz]
        for w in self.walls:
            if w.dist_to_wall_inner(pos1_xyz)<0.0:
                continue
            canStop,isBlock,toPos=w.block_wall(poses[-1],pos2_xyz)
            if isBlock:
                poses.append(toPos)
            elif canStop:
                poses.append(pos2_xyz)
                break
        if poses[-1]!=pos2_xyz:
            poses.append(pos2_xyz)
        #adjust vertices' y coordinates
        alpha=0
        dpos=(pos2_xyz[0]-pos1_xyz[0],pos2_xyz[1]-pos1_xyz[1],pos2_xyz[2]-pos1_xyz[2])
        crd=2 if abs(dpos[2])>abs(dpos[0]) else 0
        for i in range(1,len(poses)-1):
            alpha=(poses[i][crd]-poses[0][crd])/dpos[crd]
            poses[i]=(poses[i][0],dpos[1]*alpha+poses[0][1],poses[i][2])
        #calculate distance
        dist=0
        for i in range(0,len(poses)-1):
            a,b=poses[i],poses[i+1]
            dist+=np.linalg.norm([a[0]-b[0],a[1]-b[1],a[2]-b[2]])
        #compute distance
        return dist,poses

    def test_scene_feature(self, nr_iter):
        if os.path.exists('test_scene_feature'):
            shutil.rmtree('test_scene_feature')
        os.mkdir('test_scene_feature')
        random.seed(0)
        #x
        min_x=self.floors[0].min_x
        max_x=self.floors[0].max_x
        for f in self.floors:
            min_x=min(min_x,f.min_x)
            max_x=max(max_x,f.max_x)
        #y
        min_y=-self.width_floor
        max_y=self.width_floor
        #z
        min_z=self.walls[0].min_z
        max_z=self.walls[0].max_z
        for w in self.walls:
            min_z=min(min_z,w.min_z)
            max_z=max(max_z,w.max_z)
        #test
        for i in range(nr_iter):
            pos=(random.uniform(min_x,max_x),random.uniform(min_y,max_y),random.uniform(min_z,max_z))
            dist_f=self.floor_dist(pos)
            dist_w=self.wall_dist(pos)
            pts=[pos,(pos[0],pos[1],pos[2]-dist_f),pos,(pos[0]+dist_w,pos[1],pos[2])]
            self.write_pts_vtk(pts,"test_scene_feature/frm"+str(i)+".vtk")
    
    def test_scene_geodesic(self, nr_iter):
        if os.path.exists('test_scene_geodesic'):
            shutil.rmtree('test_scene_geodesic')
        os.mkdir('test_scene_geodesic')
        random.seed(0)
        #x
        min_x=self.floors[0].min_x
        max_x=self.floors[0].max_x
        for f in self.floors:
            min_x=min(min_x,f.min_x)
            max_x=max(max_x,f.max_x)
        #y
        min_y=-self.width_floor
        max_y=self.width_floor
        #z
        min_z=self.walls[0].min_z
        max_z=self.walls[0].max_z
        for w in self.walls:
            min_z=min(min_z,w.min_z)
            max_z=max(max_z,w.max_z)
        #test
        for i in range(nr_iter):
            #pos0
            pos0=(0,0,0)
            while True:
                pos0=(random.uniform(min_x,max_x),random.uniform(min_y,max_y),random.uniform(min_z,max_z))
                dist_f=self.floor_dist(pos0)
                dist_w=self.wall_dist(pos0)
                if dist_f>0 and dist_f<self.sz_floor and dist_w>0 and dist_w<self.sz_wall:
                    break
            #pos1
            pos1=(0,0,0)
            while True:
                pos1=(random.uniform(min_x,max_x),random.uniform(min_y,max_y),random.uniform(min_z,max_z))
                dist_f=self.floor_dist(pos1)
                dist_w=self.wall_dist(pos1)
                if dist_f>0 and dist_f<self.sz_floor and dist_w>0 and dist_w<self.sz_wall:
                    break
            #geod_dist
            dist,poses=self.calc_geodesic_distance(pos0,pos1)
            #distRef=0
            pts=[]
            for j in range(0,len(poses)-1):
                a,b=poses[j],poses[j+1]
                pts.append(a)
                pts.append(b)
                #distRef+=np.linalg.norm(np.array([a[0]-b[0],a[1]-b[1],a[2]-b[2]]))
            #print('dist: %f err: %f'%(dist,distRef-dist))
            self.write_pts_vtk(pts,"test_scene_geodesic/frm"+str(i)+".vtk")
    
    def write_pts_vtk(self, pts, path):
        #write a vtk with a point and two lines
        f=open(path, "w")
        f.write("# vtk DataFile Version 1.0\n")
        f.write("scene feature\n")
        f.write("ASCII\n")
        f.write("\n")
        f.write("DATASET POLYDATA\n")
        f.write("POINTS "+str(len(pts))+" double\n")
        for i in range(0,len(pts),2):
            f.write(str(pts[i+0][0])+" "+str(pts[i+0][1])+" "+str(pts[i+0][2])+"\n")
            f.write(str(pts[i+1][0])+" "+str(pts[i+1][1])+" "+str(pts[i+1][2])+"\n")
        f.write("\n")
        f.write("LINES "+str(len(pts)//2)+" "+str(len(pts)//2*3)+"\n")
        for i in range(0,len(pts)//2):
            f.write("2 "+str(i*2+0)+" "+str(i*2+1)+"\n")
        f.close()
        

class RandomCliffScene(CliffScene):

    def create_cliff(self, path):
        rank = '_' + str(MPI.COMM_WORLD.Get_rank())
        root = ET.Element("sdf",version="1.6")
        world = ET.SubElement(root,"world",name="default")
        ET.SubElement(world,"gravity").text="0 0 -9.8"

        for i in range(self.nr_level):
            self.create_mesh(world,"floor"+str(i)+rank)
            self.create_mesh(world,"wall"+str(i)+rank)

        self.indent(root)
        tree = ET.ElementTree(root)
        tree.write(path)

    def episode_restart(self, bullet_client):
        rank = '_' + str(MPI.COMM_WORLD.Get_rank())
        self._p = bullet_client
        Scene.episode_restart(self, bullet_client)

        self.floors = []
        self.walls = []
        self.floor_and_wall_names=[]
        dirname = os.path.join(os.path.dirname(__file__), "..", "pybulletgym", "envs", "assets", "scenes", "cliff")
        if self.exp_name != '':
            dirname += '_' + self.exp_name
        os.makedirs(dirname, exist_ok=True)
        curr_pos = self.init_pos()
        pts=[]
        for i in range(self.nr_level):
            self.floor_and_wall_names.append("floor"+str(i)+rank)
            self.floor_and_wall_names.append("wall"+str(i)+rank)
            #add floor
            curr_pos0 = curr_pos
            curr_pos = self.create_floor(os.path.join(dirname, "floor"+str(i)+rank+".obj"),curr_pos0,"floor")
            self.floors.append(Plane(curr_pos0, curr_pos))
            pts+=[self.floors[-1].curr_pos0,self.floors[-1].curr_pos]
            #add wall
            curr_pos0 = curr_pos
            curr_pos = self.create_wall(os.path.join(dirname, "wall" +str(i)+rank+".obj"),curr_pos0,"wall" )
            self.walls.append(Plane(curr_pos0, curr_pos))
            pts+=[self.walls[-1].curr_pos0,self.walls[-1].curr_pos]
        self.create_cliff(os.path.join(dirname, "cliff" + rank + ".sdf"))
        # self.write_pts_vtk(pts, 'cliff' + rank + '.vtk')
        self.ground_plane_mjcf=self._p.loadSDF(os.path.join(dirname, "cliff" + rank + ".sdf"))

        for i in self.ground_plane_mjcf:
            self._p.changeDynamics(i, -1, lateralFriction=0.8, restitution=0.5)
            self._p.changeVisualShape(i,-1,rgbaColor=[1,1,1,0.8])
            
        self._p.configureDebugVisualizer(pybullet.COV_ENABLE_PLANAR_REFLECTION,1)