from gym.envs.registration import register

# climbing envs
register(
    id='WalkerClimberBaseBulletEnv-v0',
    entry_point='climb_envs.env_climbing:HumanoidWalkerClimberBulletEnv',
    max_episode_steps=1000
    )

register(
    id='RandomWalkerEnv-v0',
    entry_point='climb_envs.env_climbing:RandomWalkerEnv',
    max_episode_steps=1000
    )

register(
    id='RandomClimberEnv-v0',
    entry_point='climb_envs.env_climbing:RandomClimberEnv',
    max_episode_steps=1000
    )

register(
    id='RandomStanderEnv-v0',
    entry_point='climb_envs.env_climbing:RandomStanderEnv',
    max_episode_steps=1000
    )

register(
    id='End2EndEnv-v0',
    entry_point='climb_envs.env_climbing:End2EndEnv',
    max_episode_steps=1000
    )
