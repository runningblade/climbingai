import numpy as np
import pybullet,os,pygame,math
from climb_envs.vapory import *
from pybulletgym.envs.roboschool.robot_bases import XmlBasedRobot

class RobotPovrayRenderer():
    
    def __init__(self, robot, path, antialiasing=0.6, fog_distance=20):
        self.path = path
        self._p = robot._p
        self.parts = []
        #robot parts
        for _,p in robot.parts.items():
            if p.bodies[p.bodyIndex] != robot.objects[0]:
                continue
            index = 0
            c = robot.get_color(p.bodyPartIndex)
            shapes = self._p.getCollisionShapeData(objectUniqueId=robot.objects[0],linkIndex=p.bodyPartIndex)
            if p.bodyPartIndex == -1:
                a=1
            while index < len(shapes):
                if self.is_sphere(shapes,index):
                    self.parts.append(RobotPovrayPart(pov=self, part=p, type=pybullet.GEOM_SPHERE, 
                                                      dim=list(shapes[index][-2])+[shapes[index][3][0]], color=c))
                    index=index+1
                elif self.is_capsule(shapes,index):
                    self.parts.append(RobotPovrayPart(pov=self, part=p, type=pybullet.GEOM_CAPSULE, 
                                                      dim=list(shapes[index][-2])+list(shapes[index+1][-2])+[shapes[index][3][0]], color=c))
                    index=index+2
                else: index=index+1
        #other parts
        ck = [1,1,1,1]
        for _,p in robot.parts.items():
            if p.bodies[p.bodyIndex] == robot.objects[0]:
                continue
            c = robot.get_object_color(p.bodies[p.bodyIndex])
            shapes = self._p.getCollisionShapeData(objectUniqueId=p.bodies[p.bodyIndex],linkIndex=p.bodyPartIndex)
            assert len(shapes) == 1 and shapes[0][2] == pybullet.GEOM_MESH
            self.parts.append(RobotPovrayPart(pov=self, part=p, type=pybullet.GEOM_MESH, 
                                              dim=[shapes[0][4].decode("utf8")], color=c, checker=ck))
        #parameter
        self.lights = []
        self.lights.append([[-100,0,0],[1,1,1],[100,0,0]])
        self.lights.append([[0,0,100],[1,1,1],[0,0,-100]])
        self.lights.append([[0,0,10],[0.5,0.5,0.5]])
        self.antialiasing = antialiasing
        self.fog_distance = fog_distance
    
    def render(self, env):
        #camera
        cam_info = env._p.getDebugVisualizerCamera()
        yaw = env._cam_yaw*math.pi/180.0
        pitch = -env._cam_pitch*math.pi/180.0
        cam_pos = np.array([math.sin(yaw)*math.cos(pitch), math.cos(yaw)*math.cos(pitch), math.sin(pitch)])*env._cam_dist
        cam_pos += np.array(list(cam_info[-1]))
        camera = Camera('location',cam_pos.tolist(),'look_at',cam_info[-1],'sky',cam_info[4])
        #light
        lights = []
        for l in self.lights:
            if len(l) == 3:
                lights.append(LightSource(l[0],'color',l[1],'parallel','point_at',l[2]))
            elif len(l) == 2:
                lights.append(LightSource((np.array(l[0])+np.array(list(env.robot.body_xyz))).tolist(),
                                          'color',l[1],'parallel','point_at',env.robot.body_xyz))
        #misc
        bkg = Background('color','rgb',[1,1,1])
        fog = Fog('distance',self.fog_distance,'color','rgb',[1,1,1])
        scene = Scene(camera, [p.get_script() for p in self.parts]+lights+[bkg]+[fog])
        #render
        imgPath = os.path.join(self.path,'frm'+str(env.frame)+'.png')
        povPath = os.path.join(self.path,'frm'+str(env.frame)+'.pov')
        try:
            if not os.path.exists(self.path):
                os.makedirs(self.path)
            scene.render(imgPath, tempfile=povPath, remove_temp=False, 
                         width=env._render_width, height=env._render_height, 
                         antialiasing=self.antialiasing)
        except:
            print('Error calling povray!')
        return imgPath
        
    def is_sphere(self, shapes, index):
        if shapes[index][2] != pybullet.GEOM_SPHERE:
            return False
        if index == len(shapes)-1:
            return True
        if shapes[index+1][2] != pybullet.GEOM_SPHERE:
            return True
        if abs(shapes[index+1][3][0]-shapes[index][3][0]) < 1e-5:
            return False
        return True

    def is_capsule(self, shapes, index):
        if index == len(shapes)-1:
            return False
        if shapes[index][2] != pybullet.GEOM_SPHERE or shapes[index+1][2] != pybullet.GEOM_SPHERE:
            return False
        return abs(shapes[index+1][3][0]-shapes[index][3][0]) < 1e-5
        
class RobotPovrayPart():
    
    def __init__(self, pov, part, type, dim, color, phong=0.3, specular=0.1, checker=None):
        self.pov = pov
        self.part = part
        self.type = type
        self.dim = dim
        self.color = color
        self.phong = phong
        self.specular = specular
        self.checker = checker
        if type == pybullet.GEOM_MESH:
            m = OBJ(dim[0])
            fss = []
            for f in m.faces:
                fss+=self.to_triangle([id-1 for id in list(f[0])])
            self.dim = [[list(v) for v in m.vertices], FaceIndices(len(fss),*fss)]
        
    def to_triangle(self, face):
        if len(face) == 3:
            return [face]
        else:
            assert len(face) == 4
            return [face[0],face[1],face[2]], [face[0],face[2],face[3]]
        
    def get_texture(self):
        if self.checker is not None:
            pig=Pigment('checker','rgb',self.color[0:3],
                        'rgb',self.checker[0:3],
                        'scale',self.checker[3])
        else: 
            pig = Pigment('rgb',self.color[0:3],
                          'transmit',1-self.color[3])
        finish = Finish('phong',self.phong,'specular',self.specular)
        tex = Texture(pig,finish)
        return tex
        
    def get_trans(self):
        return self.part.get_position()
        
    def get_rot(self):
        return np.array(self.part._p.getMatrixFromQuaternion(self.part.get_orientation())).reshape(3,3)
        
    def get_sphere(self):
        trans = self.get_trans()
        rot = self.get_rot()
        pos = (np.dot(rot,np.array(self.dim[0:3]))+trans).tolist()
        return Sphere(pos, self.dim[3], self.get_texture())
        
    def get_capsule(self):
        trans = self.get_trans()
        rot = self.get_rot()
        pos0 = (np.dot(rot,np.array(self.dim[0:3]))+trans).tolist()
        pos1 = (np.dot(rot,np.array(self.dim[3:6]))+trans).tolist()
        s0 = Sphere(pos0, self.dim[6])
        s1 = Sphere(pos1, self.dim[6])
        c = Cylinder(pos0, pos1, self.dim[6])
        return Union(*[s0,s1,c], self.get_texture())
        
    def get_mesh(self):
        trans = self.get_trans()
        rot = self.get_rot()
        vsst = [(np.dot(rot,v)+trans).tolist() for v in self.dim[0]]
        for v in self.dim[0]:
            vsst.append(v)
        return Mesh2(VertexVectors(len(vsst),*vsst),self.dim[1],self.get_texture())
        
    def get_script(self):
        if self.type == pybullet.GEOM_CAPSULE:
            return self.get_capsule()
        elif self.type == pybullet.GEOM_SPHERE:
            return self.get_sphere()
        elif self.type == pybullet.GEOM_MESH:
            return self.get_mesh()
        else: 
            print("Unsupported povray type!")
            assert False
            
class OBJ:
    
    def __init__(self, filename, swapyz=False):
        """Loads a Wavefront OBJ file. """
        self.vertices = []
        self.normals = []
        self.texcoords = []
        self.faces = []

        material = None
        for line in open(filename, "r"):
            if line.startswith('#'): continue
            values = line.split()
            if not values: continue
            if values[0] == 'v':
                v = map(float, values[1:4])
                if swapyz:
                    v = v[0], v[2], v[1]
                self.vertices.append(v)
            elif values[0] == 'vn':
                v = map(float, values[1:4])
                if swapyz:
                    v = v[0], v[2], v[1]
                self.normals.append(v)
            elif values[0] == 'vt':
                self.texcoords.append(map(float, values[1:3]))
            elif values[0] in ('usemtl', 'usemat'):
                material = values[1]
            elif values[0] == 'mtllib':
                self.mtl = None
            elif values[0] == 'f':
                face = []
                texcoords = []
                norms = []
                for v in values[1:]:
                    w = v.split('/')
                    face.append(int(w[0]))
                    if len(w) >= 2 and len(w[1]) > 0:
                        texcoords.append(int(w[1]))
                    else:
                        texcoords.append(0)
                    if len(w) >= 3 and len(w[2]) > 0:
                        norms.append(int(w[2]))
                    else:
                        norms.append(0)
                self.faces.append((face, norms, texcoords, material))