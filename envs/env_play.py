#this is for visualizing different bullet environments
import sys,os
sys.path.insert(0,os.path.abspath("../tools"))
import gym,argparse,time,climb_envs
from climb_envs.robot_povray_renderer import *
from pybulletgym.envs.roboschool.gym_locomotion_envs import *

#argument
parser = argparse.ArgumentParser(description='Train.')
parser.add_argument('--id', type=str, default='SlopedClimberEnv-v0')
parser.add_argument('--show_debug_info', default=False, action='store_true')
parser.add_argument('--norender', default=False, action='store_true')
parser.add_argument('--usePovray', default=False, action='store_true')
parser.add_argument('--record', type=str, default='')
parser.add_argument('--yaw_step', type=int, default=1)
parser.add_argument('--yaw_init', type=int, default=0)
parser.add_argument('--pitch_step', type=int, default=1)
parser.add_argument('--pitch_init', type=int, default=0)
parser.add_argument('--dist_step', type=float, default=0.1)
parser.add_argument('--dist_init', type=float, default=5)
parser.add_argument('--FPS', type=int, default=60)
args = parser.parse_args()

#initialize environment
env = gym.make(args.id)
dirname = os.path.dirname(os.path.abspath(__file__))
path = os.path.join(dirname,'..','results',args.id)
if isinstance(env,gym.Wrapper):
    env = env.env
if args.record!='':
    env._render_width = int(args.record.split(',')[0])
    env._render_height = int(args.record.split(',')[1])
    env = gym.wrappers.Monitor(env, path,force=True)
else: 
    env = gym.wrappers.TimeLimit(env)
env.env.isRender = not args.norender
env.env.show_debug_info = args.show_debug_info
act = env.action_space
a = (act.low+act.high)/2
if 'climb' in args.id.lower():
    a[-4]=a[-3]=a[-2]=a[-1]=-1.0

#run environment
pressed_key = {}
t=time.time()
env.reset()
#lets debug scene_cliff
env.env.cliff_scene.test_scene_feature(1000)
env.env.cliff_scene.test_scene_geodesic(1000)
exit(-1)
if args.usePovray:
    env.env.povrayRenderer = RobotPovrayRenderer(env.env.robot, path)
env.env._cam_yaw = args.yaw_init
env.env._cam_pitch = args.pitch_init
env.env._cam_dist = args.dist_init
sim = True
can_end = False
while not can_end:
    t2=time.time()
    if (t2-t) > 1/float(args.FPS):
        #keyboard
        for k,v in pybullet.getKeyboardEvents().items():
            if v==pybullet.KEY_IS_DOWN:
                if chr(k)=='[':
                    env.env._cam_yaw += args.yaw_step
                elif chr(k)==']':
                    env.env._cam_yaw -= args.yaw_step
                if chr(k)==';':
                    env.env._cam_pitch += args.pitch_step
                elif chr(k)=='\'':
                    env.env._cam_pitch -= args.pitch_step
                if chr(k)==',':
                    env.env._cam_dist += args.dist_step
                elif chr(k)=='.':
                    env.env._cam_dist -= args.dist_step
                elif chr(k)=='b':
                    can_end = True
                elif k not in pressed_key or t-pressed_key[k] > 5/float(args.FPS):
                    pressed_key[k]=t
                    if 'climb' in args.id.lower():
                        if chr(k)=='y':
                            a[-1]*=-1.0
                        elif chr(k)=='u':
                            a[-2]*=-1.0
                        elif chr(k)=='h':
                            a[-3]*=-1.0
                        elif chr(k)=='j':
                            a[-4]*=-1.0
                    if chr(k)==' ':
                        sim=not sim
                        print('Start simulation' if sim else 'Stop simulation')
                    elif chr(k)=='r':
                        env.reset()
                        a[-4]=a[-3]=a[-2]=a[-1]=-1.0
                        print('Reset')
                    elif chr(k)=='l':
                        print('Camera parameter: %d %d %f'%(env.env._cam_yaw,env.env._cam_pitch,env.env._cam_dist))
                    elif chr(k)=='\\':
                        env.env.fix_camera = not env.env.fix_camera
                        env.env._cam_yaw=env.env._p.getDebugVisualizerCamera()[-4]
                        env.env._cam_pitch=env.env._p.getDebugVisualizerCamera()[-3]
                        env.env._cam_dist=env.env._p.getDebugVisualizerCamera()[-2]
                        print('Fix camera' if env.env.fix_camera else 'Free camera')
                    elif chr(k)=='r':
                        env.env.show_debug_info = not env.env.show_debug_info
                        print('Show debug info' if env.env.show_debug_info else 'Hide debug info')
        #simulate
        if sim:
            s, r, d, i = env.step(a)
        if env.env.isRender:
            env.render(mode = 'rgb_array' if args.record else 'human')
        t=t2
        
#finalize for video streaming
env.close()